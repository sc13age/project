package sc13age;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by Attila Gergely Eke on 23/02/16.
 * Needed for the security.
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

    public SecurityWebApplicationInitializer() {
        super(WebSecurityConfig.class);
    }
}