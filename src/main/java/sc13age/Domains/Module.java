package sc13age.Domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Attila Gergely Eke on 24/02/16.
 * Table that represents the modules' information
 */
@Entity
@Table
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "courses", "school", "studentPreferenceFormsList"})
public class Module {

    public enum ExamType {
        ClosedBook("Closed book"),
        OpenBook("Open book"),
        NoExam("No exam");

        private final String displayName;

        private ExamType(String displayName) {
            this.displayName = displayName;
        }

        public String getDisplayName() {
            return this.displayName;
        }
    };

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column
    private String moduleCode;

    @Column
    private String name;

    @Column
    private Integer credits;

    @Column
    private String year;

    @Column
    private Integer level;

    @Column
    private String lecturer;

    @Column
    private Double feedbackScore;

    @OneToMany(mappedBy = "module", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Prerequisite> prerequisites;

    @OneToMany(mappedBy = "module", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Tag> tags;

    @OneToMany(mappedBy = "module", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Course> courses;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private School school;

    @OneToMany(mappedBy = "module", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<StudentPreferenceForms> studentPreferenceFormsList;

    private Integer numberOfOptionalPrerequisitsNeeded;

    @Column(nullable = false, columnDefinition = "BOOLEAN")
    private Boolean discoveryModule = Boolean.FALSE;

    @Column(nullable = false, columnDefinition = "BOOLEAN")
    private Boolean skillsDiscoveryModule = Boolean.FALSE;

    @Column(nullable = false, columnDefinition = "BOOLEAN")
    private Boolean availableForIncomingStudyAbroad = Boolean.FALSE;

    private String moduleCatalogueLink;

    private Integer examCount;

    private Integer cwCount;

    @Type(type="text")
    private String objectives;

    @Type(type="text")
    private String syllabus;

    @Type(type="text")
    private String learningOutcomes;

    @Type(type="text")
    private String skillsOutcomes;

    @Type(type="text")
    private String moduleSummary;

    @Enumerated(EnumType.STRING)
    private ExamType examType;

    public Double getFeedbackScore() {
        return feedbackScore;
    }

    public void setFeedbackScore(Double feedbackScore) {
        this.feedbackScore = feedbackScore;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    @JsonManagedReference
    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public List<StudentPreferenceForms> getStudentPreferenceFormsList() {
        return studentPreferenceFormsList;
    }

    public void setStudentPreferenceFormsList(List<StudentPreferenceForms> studentPreferenceFormsList) {
        this.studentPreferenceFormsList = studentPreferenceFormsList;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    @JsonManagedReference
    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @JsonManagedReference
    public List<Prerequisite> getPrerequisites() {
        return prerequisites;
    }

    public void setPrerequisites(List<Prerequisite> prerequisites) {
        this.prerequisites = prerequisites;
    }

    public Integer getNumberOfOptionalPrerequisitsNeeded() {
        return numberOfOptionalPrerequisitsNeeded;
    }

    public void setNumberOfOptionalPrerequisitsNeeded(Integer numberOfOptionalPrerequisitsNeeded) {
        this.numberOfOptionalPrerequisitsNeeded = numberOfOptionalPrerequisitsNeeded;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Boolean getDiscoveryModule() {
        return discoveryModule;
    }

    public void setDiscoveryModule(Boolean discoveryModule) {
        this.discoveryModule = discoveryModule;
    }

    public Boolean getSkillsDiscoveryModule() {
        return skillsDiscoveryModule;
    }

    public void setSkillsDiscoveryModule(Boolean skillsDiscoveryModule) {
        this.skillsDiscoveryModule = skillsDiscoveryModule;
    }

    public Boolean getAvailableForIncomingStudyAbroad() {
        return availableForIncomingStudyAbroad;
    }

    public void setAvailableForIncomingStudyAbroad(Boolean availableForIncomingStudyAbroad) {
        this.availableForIncomingStudyAbroad = availableForIncomingStudyAbroad;
    }

    public String getModuleCatalogueLink() {
        return moduleCatalogueLink;
    }

    public void setModuleCatalogueLink(String moduleCatalogueLink) {
        this.moduleCatalogueLink = moduleCatalogueLink;
    }

    public Integer getExamCount() {
        return examCount;
    }

    public void setExamCount(Integer examCount) {
        this.examCount = examCount;
    }

    public Integer getCwCount() {
        return cwCount;
    }

    public void setCwCount(Integer cwCount) {
        this.cwCount = cwCount;
    }

    public String getObjectives() {
        return objectives;
    }

    public void setObjectives(String objectives) {
        this.objectives = objectives;
    }

    public String getSyllabus() {
        return syllabus;
    }

    public void setSyllabus(String syllabus) {
        this.syllabus = syllabus;
    }

    public ExamType getExamType() {
        return examType;
    }

    public void setExamType(ExamType examType) {
        this.examType = examType;
    }

    public String getLearningOutcomes() {
        return learningOutcomes;
    }

    public void setLearningOutcomes(String learningOutcomes) {
        this.learningOutcomes = learningOutcomes;
    }

    public String getSkillsOutcomes() {
        return skillsOutcomes;
    }

    public void setSkillsOutcomes(String skillsOutcomes) {
        this.skillsOutcomes = skillsOutcomes;
    }

    public String getModuleSummary() {
        return moduleSummary;
    }

    public void setModuleSummary(String moduleSummary) {
        this.moduleSummary = moduleSummary;
    }
}
