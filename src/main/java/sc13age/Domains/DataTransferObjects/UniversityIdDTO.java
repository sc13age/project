package sc13age.Domains.DataTransferObjects;

/**
 * Created by Attila Gergely Eke on 28/03/16.
 */
public class UniversityIdDTO {
    private Long university;

    public Long getUniversity() {
        return university;
    }

    public void setUniversity(Long university) {
        this.university = university;
    }
}
