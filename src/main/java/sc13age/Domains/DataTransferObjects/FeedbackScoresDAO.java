package sc13age.Domains.DataTransferObjects;

/**
 * Created by Attila Gergely Eke on 04/05/16.
 */
public class FeedbackScoresDAO {

    public FeedbackScoresDAO(Double feedbackScore, Double id) {
        this.feedbackScore = feedbackScore;
        this.id = id;
    }

    public FeedbackScoresDAO(){}

    private Double feedbackScore;

    private Double id;

    public Double getFeedbackScore() {
        return feedbackScore;
    }

    public void setFeedbackScore(Double feedbackScore) {
        this.feedbackScore = feedbackScore;
    }

    public Double getId() {
        return id;
    }

    public void setId(Double id) {
        this.id = id;
    }
}
