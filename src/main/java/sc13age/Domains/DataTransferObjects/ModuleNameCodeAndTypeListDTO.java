package sc13age.Domains.DataTransferObjects;

import sc13age.Domains.DataTransferObjects.ModuleNameCodeAndTypeDTO;

import java.util.List;

/**
 * Created by Attila Gergely Eke on 21/03/16.
 */
public class ModuleNameCodeAndTypeListDTO {
    public ModuleNameCodeAndTypeListDTO(List<ModuleNameCodeAndTypeDTO> moduleNameCodeAndTypeDTOs) {
        this.moduleNameCodeAndTypeDTOs = moduleNameCodeAndTypeDTOs;
    }

    public ModuleNameCodeAndTypeListDTO(){}

    List<ModuleNameCodeAndTypeDTO> moduleNameCodeAndTypeDTOs;

    public List<ModuleNameCodeAndTypeDTO> getModuleNameCodeAndTypeDTOs() {
        return moduleNameCodeAndTypeDTOs;
    }

    public void setModuleNameCodeAndTypeDTOs(List<ModuleNameCodeAndTypeDTO> moduleNameCodeAndTypeDTOs) {
        this.moduleNameCodeAndTypeDTOs = moduleNameCodeAndTypeDTOs;
    }
}
