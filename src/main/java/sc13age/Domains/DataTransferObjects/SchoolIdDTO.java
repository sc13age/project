package sc13age.Domains.DataTransferObjects;

/**
 * Created by Attila Gergely Eke on 28/03/16.
 */
public class SchoolIdDTO {
    private Long school;

    public Long getSchool() {
        return school;
    }

    public void setSchool(Long school) {
        this.school = school;
    }
}
