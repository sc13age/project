package sc13age.Domains.DataTransferObjects;

import sc13age.Domains.AvailableTag;
import sc13age.Domains.Module;

import java.util.List;

/**
 * Created by Attila Gergely Eke on 15/03/16.
 */
public class ModulesDTO {

    private List<Module> modules;

    private List<AvailableTag> tags;

    public ModulesDTO(List<Module> modules, List<AvailableTag> tags) {
        this.modules = modules;
        this.tags = tags;
    }

    public List<Module> getModules() {
        return modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

    public List<AvailableTag> getTags() {
        return tags;
    }

    public void setTags(List<AvailableTag> tags) {
        this.tags = tags;
    }
}
