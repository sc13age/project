package sc13age.Domains.DataTransferObjects;

import javax.persistence.*;

/**
 * Created by Attila Gergely Eke on 21/03/16.
 */
public class ModuleNameCodeAndTypeDTO {
    public ModuleNameCodeAndTypeDTO(String name, String moduleCode, String type) {
        this.name = name;
        this.moduleCode = moduleCode;
        this.type = type;
    }

    public ModuleNameCodeAndTypeDTO(){}

    private String name;

    private String moduleCode;

    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
