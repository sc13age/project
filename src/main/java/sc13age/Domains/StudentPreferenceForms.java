package sc13age.Domains;

import sc13age.Domains.PreferenceFormParts.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Attila Gergely Eke on 24/02/16.
 * Table that represents the student feedback form results for each module and year.
 */
@Entity
public class StudentPreferenceForms {

    public enum ResponseType {
        DefinitelyAgree("Definitely Agree"),
        MostlyAgree("Mostly Agree"),
        Neutral("Neither Agree Nor Disagree"),
        MostlyDisagree("Mostly Disagree"),
        DefinitelyDisagree("Definitely Disagree");

        private final String displayName;

        private ResponseType(String displayName) {
            this.displayName = displayName;
        }

        public String getDisplayName() {
            return this.displayName;
        }
    };

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column
    private Integer year;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Module module;

    @OneToMany(mappedBy = "spf", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ContentIntellectuallyStimulating> contentIntellectuallyStimulatingList;

    @OneToMany(mappedBy = "spf", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FeedbackOnWrittenWorkUseful> feedbackOnWrittenWorkUsefulList;

    @OneToMany(mappedBy = "spf", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FullyEngagedWithModule> fullyEngagedWithModuleList;

    @OneToMany(mappedBy = "spf", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<MaterialsResourcesHighStandard> materialsResourcesHighStandardList;

    @OneToMany(mappedBy = "spf", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SatisfiedWithModuleQuality> satisfiedWithModuleQualityList;

    @OneToMany(mappedBy = "spf", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<StaffMadeSubjectInteresting> staffMadeSubjectInterestingList;

    @OneToMany(mappedBy = "spf", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<StaffWereEnthusiastic> staffWereEnthusiasticList;

    @OneToMany(mappedBy = "spf", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<StaffWereGoodAtExplaining> staffWereGoodAtExplainingList;

    @OneToMany(mappedBy = "spf", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SuffGuidanceAvailWhenNeeded> suffGuidanceAvailWhenNeededList;

    private Double cis;

    private Double fowwu;

    private Double fewm;

    private Double mrhs;

    private Double swmq;

    private Double smsi;

    private Double swe;

    private Double swgae;

    private Double sgawn;

    public Double getCis() {
        return cis;
    }

    public void setCis(Double cis) {
        this.cis = cis;
    }

    public Double getFowwu() {
        return fowwu;
    }

    public void setFowwu(Double fowwu) {
        this.fowwu = fowwu;
    }

    public Double getFewm() {
        return fewm;
    }

    public void setFewm(Double fewm) {
        this.fewm = fewm;
    }

    public Double getMrhs() {
        return mrhs;
    }

    public void setMrhs(Double mrhs) {
        this.mrhs = mrhs;
    }

    public Double getSwmq() {
        return swmq;
    }

    public void setSwmq(Double swmq) {
        this.swmq = swmq;
    }

    public Double getSmsi() {
        return smsi;
    }

    public void setSmsi(Double smsi) {
        this.smsi = smsi;
    }

    public Double getSwe() {
        return swe;
    }

    public void setSwe(Double swe) {
        this.swe = swe;
    }

    public Double getSwgae() {
        return swgae;
    }

    public void setSwgae(Double swgae) {
        this.swgae = swgae;
    }

    public Double getSgawn() {
        return sgawn;
    }

    public void setSgawn(Double sgawn) {
        this.sgawn = sgawn;
    }

    private Double averageResult;

    public Double getAverageResult() {
        return averageResult;
    }

    public void setAverageResult(Double averageResult) {
        this.averageResult = averageResult;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public List<ContentIntellectuallyStimulating> getContentIntellectuallyStimulatingList() {
        return contentIntellectuallyStimulatingList;
    }

    public void setContentIntellectuallyStimulatingList(List<ContentIntellectuallyStimulating> contentIntellectuallyStimulatingList) {
        this.contentIntellectuallyStimulatingList = contentIntellectuallyStimulatingList;
    }

    public List<FeedbackOnWrittenWorkUseful> getFeedbackOnWrittenWorkUsefulList() {
        return feedbackOnWrittenWorkUsefulList;
    }

    public void setFeedbackOnWrittenWorkUsefulList(List<FeedbackOnWrittenWorkUseful> feedbackOnWrittenWorkUsefulList) {
        this.feedbackOnWrittenWorkUsefulList = feedbackOnWrittenWorkUsefulList;
    }

    public List<FullyEngagedWithModule> getFullyEngagedWithModuleList() {
        return fullyEngagedWithModuleList;
    }

    public void setFullyEngagedWithModuleList(List<FullyEngagedWithModule> fullyEngagedWithModuleList) {
        this.fullyEngagedWithModuleList = fullyEngagedWithModuleList;
    }

    public List<MaterialsResourcesHighStandard> getMaterialsResourcesHighStandardList() {
        return materialsResourcesHighStandardList;
    }

    public void setMaterialsResourcesHighStandardList(List<MaterialsResourcesHighStandard> materialsResourcesHighStandardList) {
        this.materialsResourcesHighStandardList = materialsResourcesHighStandardList;
    }

    public List<SatisfiedWithModuleQuality> getSatisfiedWithModuleQualityList() {
        return satisfiedWithModuleQualityList;
    }

    public void setSatisfiedWithModuleQualityList(List<SatisfiedWithModuleQuality> satisfiedWithModuleQualityList) {
        this.satisfiedWithModuleQualityList = satisfiedWithModuleQualityList;
    }

    public List<StaffMadeSubjectInteresting> getStaffMadeSubjectInterestingList() {
        return staffMadeSubjectInterestingList;
    }

    public void setStaffMadeSubjectInterestingList(List<StaffMadeSubjectInteresting> staffMadeSubjectInterestingList) {
        this.staffMadeSubjectInterestingList = staffMadeSubjectInterestingList;
    }

    public List<StaffWereEnthusiastic> getStaffWereEnthusiasticList() {
        return staffWereEnthusiasticList;
    }

    public void setStaffWereEnthusiasticList(List<StaffWereEnthusiastic> staffWereEnthusiasticList) {
        this.staffWereEnthusiasticList = staffWereEnthusiasticList;
    }

    public List<StaffWereGoodAtExplaining> getStaffWereGoodAtExplainingList() {
        return staffWereGoodAtExplainingList;
    }

    public void setStaffWereGoodAtExplainingList(List<StaffWereGoodAtExplaining> staffWereGoodAtExplainingList) {
        this.staffWereGoodAtExplainingList = staffWereGoodAtExplainingList;
    }

    public List<SuffGuidanceAvailWhenNeeded> getSuffGuidanceAvailWhenNeededList() {
        return suffGuidanceAvailWhenNeededList;
    }

    public void setSuffGuidanceAvailWhenNeededList(List<SuffGuidanceAvailWhenNeeded> suffGuidanceAvailWhenNeededList) {
        this.suffGuidanceAvailWhenNeededList = suffGuidanceAvailWhenNeededList;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
