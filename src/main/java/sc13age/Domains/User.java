package sc13age.Domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Attila Gergely Eke on 01/02/16.
 * Table that represents the users.
 */
@Entity
@Table(name="users")
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer","school", "usersPastModules", "university"})
public class User {

    @Id
    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "firstname", nullable = false)
    private String firstname;

    @Column(name = "surname", nullable = false)
    private String surname;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private University university;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private School school;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AvailableCourse course;

    @NotNull
    @Column(nullable = false, columnDefinition = "BOOLEAN")
    private Boolean enabled = Boolean.TRUE;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Authorities> authorities;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UsersPastModules> usersPastModules;

    private Integer level;

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<Authorities> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authorities> authorities) {
        this.authorities = authorities;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public AvailableCourse getCourse() {
        return course;
    }

    public void setCourse(AvailableCourse course) {
        this.course = course;
    }

    public List<UsersPastModules> getUsersPastModules() {
        return usersPastModules;
    }

    public void setUsersPastModules(List<UsersPastModules> usersPastModules) {
        this.usersPastModules = usersPastModules;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
