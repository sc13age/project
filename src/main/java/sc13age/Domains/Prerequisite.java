package sc13age.Domains;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Attila Gergely Eke on 09/03/16.
 * Table that represents the pre-requisite relations of
 * the modules. It also captures if a module has a compulsory
 * or an optional pre-requisite on another module.
 */
@Entity
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class Prerequisite {

    public enum PrerequisiteType {
        Compulsory("Compulsory"),
        Optional("Optional");

        private final String displayName;

        private PrerequisiteType(String displayName) {
            this.displayName = displayName;
        }

        public String getDisplayName() {
            return this.displayName;
        }
    };

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Module module;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Module prereqmodule;

    @Enumerated(EnumType.STRING)
    private PrerequisiteType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonBackReference
    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public PrerequisiteType getType() {
        return type;
    }

    public void setType(PrerequisiteType type) {
        this.type = type;
    }

    public Module getPrereqmodule() {
        return prereqmodule;
    }

    public void setPrereqmodule(Module prereqmodule) {
        this.prereqmodule = prereqmodule;
    }
}
