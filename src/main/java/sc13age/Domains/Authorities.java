package sc13age.Domains;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Attila Gergely Eke on 23/02/16.
 * Table that represents the roles of the users.
 */
@Entity
public class Authorities {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull
    @JoinColumn(name="username")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User user;

    private String authority;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
