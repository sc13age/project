package sc13age.Domains.PreferenceFormParts;

import sc13age.Domains.StudentPreferenceForms;

import javax.persistence.*;

/**
 * Created by Attila Gergely Eke on 25/02/16.
 */
@Entity
public class MaterialsResourcesHighStandard {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private StudentPreferenceForms spf;

    @Column
    @Enumerated(EnumType.ORDINAL)
    private StudentPreferenceForms.ResponseType response;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StudentPreferenceForms getSpf() {
        return spf;
    }

    public void setSpf(StudentPreferenceForms spf) {
        this.spf = spf;
    }

    public StudentPreferenceForms.ResponseType getResponse() {
        return response;
    }

    public void setResponse(StudentPreferenceForms.ResponseType response) {
        this.response = response;
    }
}
