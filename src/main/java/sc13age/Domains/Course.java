package sc13age.Domains;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Attila Gergely Eke on 08/03/16.
 * Table that represents which modules are in
 * the different courses.
 */
@Entity
public class Course {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Module module;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AvailableCourse availableCourse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public AvailableCourse getAvailableCourse() {
        return availableCourse;
    }

    public void setAvailableCourse(AvailableCourse availableCourse) {
        this.availableCourse = availableCourse;
    }
}
