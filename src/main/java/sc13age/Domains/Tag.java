package sc13age.Domains;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Attila Gergely Eke on 08/03/16.
 * Table that represents the modules' attribute ratios.
 */
@Entity
public class Tag {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Module module;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AvailableTag availableTag;

    private Double percentage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonBackReference
    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public AvailableTag getAvailableTag() {
        return availableTag;
    }

    public void setAvailableTag(AvailableTag availableTag) {
        this.availableTag = availableTag;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }
}
