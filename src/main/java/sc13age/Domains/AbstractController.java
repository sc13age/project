package sc13age.Domains;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;
import sc13age.Services.UserService;

/**
 * Created by Attila Gergely Eke
 *
 * AbstractController
 *
 * Helps getting the logged in user.
 */
public class AbstractController
{
	@Autowired
	private UserService userService;

    @ModelAttribute("user")
	public User loggedInUser() {
		return userService.findOne(loggedInUserId());
	}

	public String loggedInUserId() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
     	return auth.getName();
	}
}