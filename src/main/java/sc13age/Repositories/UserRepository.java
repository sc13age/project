package sc13age.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sc13age.Domains.User;

/**
 * Created by Attila Gergely Eke on 23/02/16.
 */
public interface UserRepository extends JpaRepository<User,String> {

}
