package sc13age.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import sc13age.Domains.Module;
import sc13age.Domains.School;
import sc13age.Domains.User;

import java.util.List;

/**
 * Created by Attila Gergely Eke on 23/02/16.
 */
public interface SchoolRepository extends JpaRepository<School,String> {

    School findOneById(Long school);

    @Query(value = "select * from school " +
            "where university_id=:university",nativeQuery = true)
    List<School>findByUniversityId(@Param("university") Long university);
}
