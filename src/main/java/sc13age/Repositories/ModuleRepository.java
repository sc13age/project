package sc13age.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import sc13age.Domains.DataTransferObjects.FeedbackScoresDAO;
import sc13age.Domains.Module;
import sc13age.Domains.DataTransferObjects.ModuleNameCodeAndTypeDTO;

import java.util.List;

/**
 * Created by Attila Gergely Eke on 23/02/16.
 */
public interface ModuleRepository extends JpaRepository<Module,String> {


    /**
     * feedbackValues
     *
     * @return The feedback scores that it calculates and the module id for each module.
     * If two or more years of data is available then it weights the average of the
     * latest year by 0.6 and the rest's average with 0.4.
     */
    @Query(value = "SELECT sum(weighted_average) as feedbackScore, module_id as id FROM (\n" +
            "SELECT module_id, avg(average_result), weight,\n" +
            " avg(average_result) * weight as weighted_average\n" +
            "FROM( \n" +
            "    SELECT student_preference_forms.*,\n" +
            "        if(number_of_lines is null, 0.4, \n" +
            "           if(number_of_lines = 1 , 1, 0.6)\n" +
            "        ) as weight\n" +
            "    FROM student_preference_forms\n" +
            "    Left join(\n" +
            "        select min(id) as id, count(id) as number_of_lines ,module_id \n" +
            "        from student_preference_forms\n" +
            "        group by module_id \n" +
            "    ) tmp on tmp.id = student_preference_forms.id\n" +
            ") final\n" +
            "group by module_id , weight\n" +
            "order by module_id ASC, weight DESC)final \n" +
            "GROUP by module_id",nativeQuery = true)
    List<Object[]> feedbackValues();


    /**
     * getAllModulesFromCourse
     *
     * @param courseId
     * @param level
     * @return The modules which are in the course and level
     * that was supplied.
     */
    @Query(value = "select * from module inner join course \n" +
            "on module.id = course.module_id \n" +
            "inner join available_course \n" +
            "on available_course.id = course.available_course_id\n" +
            "where available_course.id = :courseId \n" +
            "and module.level <= :level",nativeQuery = true)
    List<Module> getAllModulesFromCourse(@Param("courseId") Long courseId,
                                         @Param("level") Integer level);

    /**
     * getModulesThatHaveAPrerequisiteRelationOnThis
     *
     * @param moduleCode
     * @param schoolId
     * @return Those modules that have a pre-requisite relation
     * on the supplied module.
     */
    @Query(value = "select module.module_code as moduleCode, \n" +
            "module.name as name, prerequisite.type as type \n" +
            "from module inner join prerequisite \n" +
            "on module.id = prerequisite.module_id \n" +
            "inner join module as module2 \n" +
            "on prerequisite.prereqmodule_id = module2.id \n" +
            "where module2.module_code = :moduleCode \n" +
            "and module.school_id = :schoolId",nativeQuery = true)
    List<ModuleNameCodeAndTypeDTO>
    getModulesThatHaveAPrerequisiteRelationOnThis(
            @Param("moduleCode") String moduleCode,
            @Param("schoolId") Long schoolId);


    /**
     * getPossibleModules
     *
     * @param moduleYear
     * @param moduleLevel
     * @param user
     * @param course
     * @return The possible modules that the user
     * can take.
     */
    @Query(value = "select module.* from module\n" +
            "left join prerequisite on module.id = prerequisite.module_id\n" +
            "left join module as module2 on prerequisite.prereqmodule_id = module2.id\n" +
            "left join course on module.id = course.module_id\n"+
            "where (module2.module_code in (\n" +
            "   select m2.module_code from users\n" +
            "   inner join users_past_modules \n" +
            "   on users.username = users_past_modules.user_username \n" +
            "   left join module as m2 on m2.id = users_past_modules.module_id\n" +
            "   where users.username=:user) \n" +
            "or coalesce(module2.module_code, '') = '')\n" +
            "and module.year = :moduleYear \n" +
            "and module.level = :moduleLevel \n" +
            "and course.available_course_id = :course \n" +
            "group by module.module_code",nativeQuery = true)
    List<Module> getPossibleModules(@Param("moduleYear") String moduleYear,
                                           @Param("moduleLevel") int moduleLevel,
                                           @Param("user") String user,
                                           @Param("course")Long course);


    Module findOneById(Long j);
}
