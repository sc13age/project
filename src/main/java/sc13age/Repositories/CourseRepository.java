package sc13age.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import sc13age.Domains.Course;
import sc13age.Domains.School;

import java.util.List;

/**
 * Created by Attila Gergely Eke on 23/02/16.
 */
public interface CourseRepository extends JpaRepository<Course,String> {



}
