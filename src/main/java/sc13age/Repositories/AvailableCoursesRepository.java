package sc13age.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import sc13age.Domains.AvailableCourse;
import sc13age.Domains.AvailableTag;
import sc13age.Domains.Course;

import java.util.List;

/**
 * Created by Attila Gergely Eke.
 */
public interface AvailableCoursesRepository extends JpaRepository<AvailableCourse,String> {
    AvailableCourse findOneById(Long course);

    @Query(value = "select * from available_course \n" +
            "where school_id=:school",nativeQuery = true)
    List<AvailableCourse> findBySchoolId(@Param("school") Long school);
}