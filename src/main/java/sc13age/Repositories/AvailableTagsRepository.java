package sc13age.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sc13age.Domains.Authorities;
import sc13age.Domains.AvailableTag;

/**
 * Created by Attila Gergely Eke.
 */
public interface AvailableTagsRepository extends JpaRepository<AvailableTag,String> {}