package sc13age.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sc13age.Domains.Authorities;

/**
 * Created by Attila Gergely Eke.
 */
public interface AuthoritiesRepository extends JpaRepository<Authorities,String> {}