package sc13age.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sc13age.Domains.AvailableTag;
import sc13age.Domains.UsersPastModules;

/**
 * Created by Attila Gergely Eke.
 */
public interface UsersPastModulesRepository extends JpaRepository<UsersPastModules,String> {}