package sc13age.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sc13age.Domains.Prerequisite;

/**
 * Created by Attila Gergely Eke.
 */
public interface PrerequisitesRepository extends JpaRepository<Prerequisite,String> {}
