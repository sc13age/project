package sc13age.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Attila Gergely Eke.
 * Averages each question result from the past feedback forms.
 */
public interface StudentPreferenceFormsRepository extends JpaRepository<sc13age.Domains.StudentPreferenceForms,String> {

    @Query("SELECT avg(c.response)\n" +
            " from StudentPreferenceForms p, ContentIntellectuallyStimulating c\n" +
            " where p.id = c.spf\n" +
            " group by c.spf")
    List<Double> getCisAverage();

    @Query("SELECT avg(c.response)\n" +
            " from StudentPreferenceForms p, FeedbackOnWrittenWorkUseful c\n" +
            " where p.id = c.spf\n" +
            " group by c.spf")
    List<Double> getFowwuAverage();

    @Query("SELECT avg(c.response)\n" +
            " from StudentPreferenceForms p, FullyEngagedWithModule c\n" +
            " where p.id = c.spf\n" +
            " group by c.spf")
    List<Double> getFewmAverage();

    @Query("SELECT avg(c.response)\n" +
            " from StudentPreferenceForms p, MaterialsResourcesHighStandard c\n" +
            " where p.id = c.spf\n" +
            " group by c.spf")
    List<Double> getMrhsAverage();

    @Query("SELECT avg(c.response)\n" +
            " from StudentPreferenceForms p, SatisfiedWithModuleQuality c\n" +
            " where p.id = c.spf\n" +
            " group by c.spf")
    List<Double> getSwmqAverage();

    @Query("SELECT avg(c.response)\n" +
            " from StudentPreferenceForms p, StaffMadeSubjectInteresting c\n" +
            " where p.id = c.spf\n" +
            " group by c.spf")
    List<Double> getSmsiAverage();

    @Query("SELECT avg(c.response)\n" +
            " from StudentPreferenceForms p, StaffWereEnthusiastic c\n" +
            " where p.id = c.spf\n" +
            " group by c.spf")
    List<Double> getSweAverage();

    @Query("SELECT avg(c.response)\n" +
            " from StudentPreferenceForms p, StaffWereGoodAtExplaining c\n" +
            " where p.id = c.spf\n" +
            " group by c.spf")
    List<Double> getSwgaeAverage();

    @Query("SELECT avg(c.response)\n" +
            " from StudentPreferenceForms p, SuffGuidanceAvailWhenNeeded c\n" +
            " where p.id = c.spf\n" +
            " group by c.spf")
    List<Double> getSgawnAverage();

}