package sc13age.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sc13age.Domains.University;
import sc13age.Domains.User;

import java.util.List;

/**
 * Created by Attila Gergely Eke on 23/02/16.
 */
public interface UniversityRepository extends JpaRepository<University,String> {

    University findOneById(Long university);
}
