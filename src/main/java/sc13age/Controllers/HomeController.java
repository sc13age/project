package sc13age.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Attila Gergely Eke on 31/01/16.
 */
@Controller
public class HomeController {

    /**
     * index
     * Returns the landing page
     * @return index page
     */
    @RequestMapping(value = "/")
    public String index() {
        return "index";
    }
}
