package sc13age.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sc13age.Domains.*;
import sc13age.Domains.DataTransferObjects.ModuleNameCodeAndTypeDTO;
import sc13age.Domains.DataTransferObjects.ModuleNameCodeAndTypeListDTO;
import sc13age.Domains.DataTransferObjects.ModulesDTO;
import sc13age.Repositories.AvailableTagsRepository;
import sc13age.Repositories.ModuleRepository;
import sc13age.Repositories.StudentPreferenceFormsRepository;
import sc13age.Repositories.UsersPastModulesRepository;
import sc13age.Services.PreferenceFormService;
import sc13age.Services.UserService;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Attila Gergely Eke on 24/02/16.
 */
@Controller
public class MainController extends AbstractController{

    @Autowired
    UserService userService;
    @Autowired
    PreferenceFormService preferenceFormService;
    @Autowired
    StudentPreferenceFormsRepository studentPreferenceFormsRepository;
    @Autowired
    ModuleRepository moduleRepository;
    @Autowired
    AvailableTagsRepository availableTagsRepository;
    @Autowired
    UsersPastModulesRepository usersPastModulesRepository;

    /**
     * showMain
     * Renders the main page
     * @param model
     * @return main view
     */
    // todo get only the past modules, check the user's level
    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public String showMain(Map<String, Object> model) {

        University usersUniversity = loggedInUser().getUniversity();
        model.put("university", usersUniversity.getName());
        model.put("school", loggedInUser().getSchool());
        model.put("possibleModules",
                moduleRepository.getAllModulesFromCourse(
                        loggedInUser().getCourse().getId(), loggedInUser().getLevel()));

        return "main";
    }

    /**
     * generateCurrentAcademicYear
     * Using LocalDate.ofYearDay(2015, 100) as the
     * current date to emulate the 2015/2016 academic year.
     *
     * @return String containing the current academic year in
     * in a format like "2015/2016"
     */
    public String generateCurrentAcademicYear(){

          // This could be used in a production environment
//        LocalDate ld = LocalDate.now();

        // This makes sure it is always 2015/2016 academic year
        // since there is only data for that year
        LocalDate ld = LocalDate.ofYearDay(2015, 100);
        LocalDate startOfSchoolTerm = LocalDate.of(ld.getYear(), 9, 28);
        if(ld.isBefore(startOfSchoolTerm)){
            return ld.getYear() + "/"
                    + ld.plusYears(1).getYear();
        }
        return ld.plusYears(1).getYear() + "/"
                + ld.plusYears(2).getYear();
    }

    /**
     * getModulesForInterfaces
     * Gets all the modules that the user can take
     * for next year based on the modules the user had taken
     * previously.
     * @return ModulesDTO a wrapper object that contains those modules
     */
    @RequestMapping(value = "/main/modules", method = RequestMethod.GET)
    public @ResponseBody ModulesDTO getModulesForInterfaces() {
        User user = loggedInUser();
        Integer levelToShow = user.getLevel() + 1;
        List<Module> m = moduleRepository.getPossibleModules(
                generateCurrentAcademicYear(), levelToShow,
                user.getUsername(), user.getCourse().getId());

        for(Iterator<Module> it = m.iterator(); it.hasNext();){
            Module m2 = it.next();
            int counter3 = 0;
            if(m2.getNumberOfOptionalPrerequisitsNeeded()>0){
                for(Prerequisite p:m2.getPrerequisites()){
                    if(p.getType()== Prerequisite.PrerequisiteType.Optional){
                        for(UsersPastModules s:user.getUsersPastModules()){
                            if(s.getModule().getModuleCode()==
                                    p.getPrereqmodule().getModuleCode()){
                                counter3 = counter3 + 1;
                            }
                        }
                    }
                }
            }
            if(m2.getNumberOfOptionalPrerequisitsNeeded()>counter3){
                System.out.println(m2.getName());
                it.remove();
            }
        }
        List<AvailableTag> availableTags = availableTagsRepository.findAll();
        ModulesDTO mdto = new ModulesDTO(m, availableTags);
        return mdto;
    }

    /**
     * getModulesThatHaveAPrerequisiteOnThisModule
     * Gets those modules that have a prerequisite relation
     * on the module that is supplied.
     * @param moduleCode 2d array containing the user's chosen module ids.
     * @return A wrapper object that contains a list of those modules.
     */
    @RequestMapping(value = "/main/prerequisite-of", method = RequestMethod.POST)
    public @ResponseBody
    ModuleNameCodeAndTypeListDTO
    getModulesThatHaveAPrerequisiteOnThisModule(@RequestBody String moduleCode) {
        User user = loggedInUser();
        moduleCode = moduleCode.substring(0,8);
        ModuleNameCodeAndTypeListDTO moduleNameCodeAndTypeListDTO =
                new ModuleNameCodeAndTypeListDTO();
        List <ModuleNameCodeAndTypeDTO> moduleNameCodeAndTypeDTO =
                moduleRepository.getModulesThatHaveAPrerequisiteRelationOnThis(
                        moduleCode, user.getSchool().getId());
        moduleNameCodeAndTypeListDTO.setModuleNameCodeAndTypeDTOs(moduleNameCodeAndTypeDTO);
        return moduleNameCodeAndTypeListDTO;
    }

    /**
     * savePastModules
     * Saves the past modules of the student
     * @param arr 2d array containing the user's chosen module ids.
     * @return String letting the client knows that everything went good.
     */
    @RequestMapping(value = "/main/savePastModules", method = RequestMethod.POST)
    public @ResponseBody String savePastModules( @RequestBody List<List<Long>> arr) {
        for(List<Long> i:arr){
            for(Long j: i){
                User user = loggedInUser();
                UsersPastModules usersPastModules = new UsersPastModules();
                usersPastModules.setModule(moduleRepository.findOneById(j));
                usersPastModules.setUser(user);
                usersPastModulesRepository.save(usersPastModules);
            }
        }
        return "Saved";
    }
}