package sc13age.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import sc13age.Domains.*;
import sc13age.Domains.DataTransferObjects.SchoolIdDTO;
import sc13age.Domains.DataTransferObjects.UniversityIdDTO;
import sc13age.Domains.Forms.RegistrationForm;
import sc13age.Repositories.AvailableCoursesRepository;
import sc13age.Repositories.CourseRepository;
import sc13age.Repositories.SchoolRepository;
import sc13age.Repositories.UniversityRepository;
import sc13age.Services.UserService;

import java.util.List;
import java.util.Map;

/**
 * Created by Attila Gergely Eke on 24/02/16.
 */
@Controller
public class RegistrationController {

    @Autowired
    UserService userService;
    @Autowired
    UniversityRepository universityRepository;
    @Autowired
    AvailableCoursesRepository availableCoursesRepository;
    @Autowired
    SchoolRepository schoolRepository;
    @Autowired
    CourseRepository courseRepository;

    /**
     * showUserRegistration
     * Renders the registration page
     * @param model injects values to the page
     * with the help of model
     * @return register view
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showUserRegistration(Map<String, Object> model) {
        model.put("universities", universityRepository.findAll());

        return "register";
    }

    /**
     * processUserRegistration
     * Sends the user's registration
     * details to the server
     * @param registrationForm, bindingResult
     * @return redirects to the login page
     * or if there's an error, returns the
     * register page
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String processUserRegistration(RegistrationForm registrationForm,
                                          BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "/register";
        }

        userService.saveUserAndAuthority(registrationForm);
        return "redirect:/login";
    }

    /**
     * getSchools
     * @param university User's university
     * @return list of all schools of the university
     */
    @RequestMapping(value = "/register/university", method = RequestMethod.POST)
    public @ResponseBody
    List<School> getSchools(@RequestBody UniversityIdDTO university) {
        return schoolRepository.findByUniversityId(university.getUniversity());
     }

    /**
     * getCourses
     * @param schoolIdDTO User's school
     * @return Returns all the available
     * courses of that school
     */
    @RequestMapping(value = "/register/school", method = RequestMethod.POST)
    public @ResponseBody
    List<AvailableCourse> getCourses(@RequestBody SchoolIdDTO schoolIdDTO) {
        return availableCoursesRepository.findBySchoolId(schoolIdDTO.getSchool());
    }
}
