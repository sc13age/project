package sc13age.Services;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import sc13age.Domains.DataTransferObjects.FeedbackScoresDAO;
import sc13age.Domains.Module;
import sc13age.Domains.StudentPreferenceForms;
import sc13age.Domains.PreferenceFormParts.ContentIntellectuallyStimulating;
import sc13age.Repositories.ModuleRepository;
import sc13age.Repositories.StudentPreferenceFormsRepository;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by Attila Gergely Eke on 05/03/16.
 */
@Service
public class PreferenceFormService {

    @Autowired
    private StudentPreferenceFormsRepository studentPreferenceFormsRepository;
    @Autowired
    private ModuleRepository moduleRepository;

    public List<StudentPreferenceForms> findAll(){
        return studentPreferenceFormsRepository.findAll();
    }

    /**
     * calculateFeedbackScores
     *
     * Calculates and saves the feedback scores
     * for the modules
     */
    public void calculateFeedbackScores(){
        List<Module> modules = moduleRepository.findAll();
        List<Object[]> fs = moduleRepository.feedbackValues();

        for(int i = 0; i < fs.size(); i++){
            Object feedbackScore = fs.get(i)[0];
            Object id = fs.get(i)[1];
            Module m = modules.get(Integer.parseInt(id.toString()) - 1);
            m.setFeedbackScore(Double.parseDouble(feedbackScore.toString()));
            moduleRepository.save(m);
        }
    }

    /**
     * averagePreferenceFormData
     *
     * Scheduled service that refreshes the modules' average
     * feedback values for each year
     */
    @Scheduled(cron = "0 0 2 * * ?")
    public void averagePreferenceFormData(){
        List<StudentPreferenceForms> spf = findAll();
        List<Double> cis = studentPreferenceFormsRepository.getCisAverage();
        List<Double> fowwu = studentPreferenceFormsRepository.getFowwuAverage();
        List<Double> fewm = studentPreferenceFormsRepository.getFewmAverage();
        List<Double> mrhs = studentPreferenceFormsRepository.getMrhsAverage();
        List<Double> swmq = studentPreferenceFormsRepository.getSwmqAverage();
        List<Double> smsi = studentPreferenceFormsRepository.getSmsiAverage();
        List<Double> swe = studentPreferenceFormsRepository.getSweAverage();
        List<Double> swgae = studentPreferenceFormsRepository.getSwgaeAverage();
        List<Double> sgawn = studentPreferenceFormsRepository.getSgawnAverage();

        int counter=0;
        for (StudentPreferenceForms i:spf){
            if(i.getYear() == 2013){
                i.setSwmq(swmq.get(counter));
                i.setFewm(fewm.get(counter));
                i.setSmsi(smsi.get(counter));
                i.setSwe(swe.get(counter));
                i.setCis(cis.get(counter));
                i.setMrhs(mrhs.get(counter));
                i.setSgawn(sgawn.get(counter));
                i.setFowwu(fowwu.get(counter));
                Double avg;
                avg=(i.getCis() + i.getFowwu() + i.getFewm()
                        + i.getMrhs() + i.getSwmq() + i.getSmsi()
                        + i.getSwe() + i.getSgawn())/8;
                i.setAverageResult(avg);
                studentPreferenceFormsRepository.save(i);
            }
            else{
            i.setCis(cis.get(counter));
            i.setFowwu(fowwu.get(counter));
            i.setFewm(fewm.get(counter));
            i.setMrhs(mrhs.get(counter));
            i.setSwmq(swmq.get(counter));
            i.setSmsi(smsi.get(counter));
            i.setSwe(swe.get(counter));
            i.setSwgae(swgae.get(counter));
            i.setSgawn(sgawn.get(counter));
            Double avg;
            avg=(i.getCis() + i.getFowwu() + i.getFewm()
                    + i.getMrhs() + i.getSwmq() + i.getSmsi()
                    + i.getSwe() + i.getSwgae() + i.getSgawn())/9;
            i.setAverageResult(avg);
            studentPreferenceFormsRepository.save(i);}
            counter=counter+1;
        }
        calculateFeedbackScores();
    }
}
