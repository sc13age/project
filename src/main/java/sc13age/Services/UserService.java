package sc13age.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sc13age.Domains.*;
import sc13age.Domains.Forms.RegistrationForm;
import sc13age.Repositories.*;

/**
 * Created by Attila Gergely Eke on 23/02/16.
 */
@Service
public class UserService
{
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthoritiesRepository authoritiesRepository;
    @Autowired
    private UniversityRepository universityRepository;
    @Autowired
    private AvailableCoursesRepository availableCoursesRepository;
    @Autowired
    private SchoolRepository schoolRepository;


    public User findOne(String username)
    {
        return userRepository.findOne(username);
    }

    public User save(User user)
    {
        return userRepository.save(user);
    }

    /**
     * saveUserAndAuthority
     * Given a form object, it saves the user into the database
     * and setting the user's authority too.
     * @param registrationForm
     */
    public void saveUserAndAuthority(RegistrationForm registrationForm){
        User user = new User();
        user.setFirstname(registrationForm.getFirstname());
        user.setSurname(registrationForm.getSurname());
        user.setPassword(registrationForm.getPassword());
        user.setUsername(registrationForm.getUsername());
        user.setLevel(registrationForm.getLevel());
        user.setUniversity(universityRepository.findOneById(registrationForm.getUniversity()));
        user.setCourse(availableCoursesRepository.findOneById(registrationForm.getCourse()));
        user.setSchool(schoolRepository.findOneById(registrationForm.getSchool()));
        userRepository.save(user);
        Authorities authority = new Authorities();
        authority.setUser(user);
        authority.setAuthority("ROLE_USER");
        authoritiesRepository.save(authority);
    }

}