How to run the application:

1. Make sure to have JAVA 8 installed. 
(In terminal write 'java -version' without the apostrophes. If it outputs
Java(TM) SE Runtime Environment (build 1.8) then it is good.)
2. Make sure that MySQL is installed. 
(If it is not then install it and set your id as 'root' and the password as 'password'
3. Clone the project from the master branch.
(In terminal write 'git clone git@gitlab.com:sc13age/project.git')
4. Go to the project's root folder. ('cd project')
5. Log into MySQL with your account in the terminal e.g. 
'mysql -u YOURUSERNAME -p' -> enter -> write your password -> enter 
6. Once you are logged in, type: 'create database project;'. 
If it is successful then close MySQL.
7. Import the database from project.sql file that can be found in the root folder.
type: 'mysql -u YOURUSERNAME -p project < project.sql'
8. If your MySQL user id and password differs from 'root' and 'password' then
go to the src/main/resources/application.properties file and modify the following
two lines to match your login details:


    spring.datasource.username=root


    spring.datasource.password=password
9. In the terminal go back to the root folder and then
execute the following command: 'mvn spring-boot:run'.
10. Finally go to http://localhost:8080/ in a browser.


Once the application is set up (Completed the phases 1 to 8) then 
subsequent application starts only need phase 9's command given that you are 
in the project's root folder.


There are some test users registered in the system. For Computer Science the id 
is user and there is no password. For IT the id is user2 and it does not have a 
password either. 


However the registration feature is working therefore you can 
create new ones.