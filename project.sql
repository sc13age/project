-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: project
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authority` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_baahryprcge2u172egph1qwur` (`username`),
  CONSTRAINT `FK_baahryprcge2u172egph1qwur` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES (1,'ROLE_USER','user'),(4,'ROLE_USER','user2');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `available_course`
--

DROP TABLE IF EXISTS `available_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `available_course` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `school_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_myqoleak8lwveq8fpqbg92t7x` (`school_id`),
  CONSTRAINT `FK_myqoleak8lwveq8fpqbg92t7x` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `available_course`
--

LOCK TABLES `available_course` WRITE;
/*!40000 ALTER TABLE `available_course` DISABLE KEYS */;
INSERT INTO `available_course` VALUES (1,'Computer Science',1),(2,'Information Technology',1);
/*!40000 ALTER TABLE `available_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `available_tag`
--

DROP TABLE IF EXISTS `available_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `available_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `available_tag`
--

LOCK TABLES `available_tag` WRITE;
/*!40000 ALTER TABLE `available_tag` DISABLE KEYS */;
INSERT INTO `available_tag` VALUES (1,'programming'),(2,'writing'),(3,'math'),(4,'logic'),(5,'userInterfaceDesign'),(6,'imageManipulation'),(7,'businessManagement'),(8,'dataQuery'),(9,'knowledgeModelling'),(10,'chartDesign'),(11,'ai'),(12,'algorithms'),(13,'graphicsModelling'),(14,'webTechnologies'),(15,'handheldTechnologies');
/*!40000 ALTER TABLE `available_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_intellectually_stimulating`
--

DROP TABLE IF EXISTS `content_intellectually_stimulating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_intellectually_stimulating` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `response` int(11) DEFAULT NULL,
  `spf_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_3h7umboyi5lpok2cpy1wiy18d` (`spf_id`),
  CONSTRAINT `FK_3h7umboyi5lpok2cpy1wiy18d` FOREIGN KEY (`spf_id`) REFERENCES `student_preference_forms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=375 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_intellectually_stimulating`
--

LOCK TABLES `content_intellectually_stimulating` WRITE;
/*!40000 ALTER TABLE `content_intellectually_stimulating` DISABLE KEYS */;
INSERT INTO `content_intellectually_stimulating` VALUES (1,0,1),(2,0,1),(3,0,1),(4,0,1),(5,0,1),(6,0,1),(7,0,1),(8,0,1),(9,0,1),(10,1,1),(11,1,1),(12,1,1),(13,1,1),(14,1,1),(15,2,1),(16,2,1),(17,2,1),(18,2,1),(19,0,2),(20,0,2),(21,1,2),(22,1,2),(23,1,2),(24,1,2),(25,1,2),(26,1,2),(27,2,2),(28,0,3),(29,0,3),(30,0,3),(31,1,3),(32,1,3),(33,1,3),(34,2,3),(35,3,3),(36,3,3),(37,4,3),(38,4,3),(39,0,4),(40,1,4),(41,0,5),(42,0,5),(43,1,5),(44,1,5),(45,1,5),(46,1,5),(47,2,5),(48,0,6),(49,0,6),(50,0,6),(51,0,6),(52,0,6),(53,1,6),(54,1,6),(55,1,6),(56,1,6),(57,1,6),(58,1,6),(59,1,6),(60,1,6),(61,2,6),(62,2,6),(63,3,6),(64,3,6),(65,3,6),(66,3,6),(67,4,6),(68,4,6),(69,4,6),(70,0,7),(71,0,7),(72,0,7),(73,1,7),(74,1,7),(75,1,7),(76,1,7),(77,1,7),(78,1,7),(79,1,7),(80,1,7),(81,1,7),(82,2,7),(83,3,7),(84,0,8),(85,0,8),(86,1,8),(87,1,8),(88,4,8),(89,0,9),(90,0,9),(91,0,9),(92,0,9),(93,0,9),(94,1,9),(95,1,9),(96,1,9),(97,1,9),(98,2,9),(99,0,10),(100,0,10),(101,0,10),(102,0,10),(103,0,10),(104,1,10),(105,1,10),(106,1,10),(107,1,10),(108,1,10),(109,2,10),(110,2,10),(111,3,10),(113,0,11),(114,0,11),(115,0,11),(116,0,11),(117,0,11),(118,0,11),(119,0,11),(120,0,11),(121,0,11),(122,0,11),(123,1,11),(124,1,11),(125,1,11),(126,1,11),(127,2,11),(128,2,11),(129,0,12),(130,0,12),(131,0,12),(132,0,12),(133,1,12),(134,1,12),(135,1,12),(136,1,12),(137,1,12),(138,2,12),(139,2,12),(140,2,12),(141,0,13),(142,0,13),(143,0,13),(144,0,13),(145,0,13),(146,0,13),(147,0,13),(148,0,13),(149,0,13),(150,0,13),(151,0,13),(152,0,13),(153,0,13),(154,0,13),(155,0,13),(156,0,13),(157,0,13),(158,1,13),(159,1,13),(160,1,13),(161,1,13),(162,1,13),(163,1,13),(164,1,13),(165,1,13),(166,1,13),(167,1,13),(168,1,13),(169,1,13),(170,1,13),(171,1,13),(172,1,13),(173,1,13),(174,1,13),(175,1,13),(176,1,13),(177,1,13),(178,2,13),(179,2,13),(180,2,13),(181,2,13),(182,3,13),(183,0,14),(184,0,14),(185,0,14),(186,0,14),(187,0,14),(188,0,14),(189,0,14),(190,0,14),(191,1,14),(192,1,14),(193,1,14),(194,1,14),(195,1,14),(196,1,14),(197,1,14),(198,1,14),(199,1,14),(200,1,14),(201,1,14),(202,1,14),(203,0,15),(204,0,15),(205,0,15),(206,0,15),(207,0,15),(208,0,15),(209,0,15),(210,0,15),(211,0,15),(212,0,15),(213,1,15),(214,1,15),(215,1,15),(216,1,15),(217,1,15),(218,1,15),(219,1,15),(220,1,15),(221,2,15),(222,2,15),(223,2,15),(224,2,15),(225,3,15),(226,4,15),(227,0,17),(228,0,17),(229,0,17),(230,0,17),(231,0,17),(232,0,17),(233,0,17),(234,0,17),(235,0,17),(236,0,17),(237,0,17),(238,4,17),(239,0,18),(240,1,18),(241,1,18),(242,1,18),(243,1,18),(244,1,18),(245,1,18),(246,1,18),(247,1,18),(248,1,18),(249,1,18),(250,1,18),(251,1,18),(252,1,18),(253,2,18),(254,2,18),(255,2,18),(256,2,18),(257,2,18),(258,2,18),(259,2,18),(260,2,18),(261,2,18),(262,3,18),(263,3,18),(264,3,18),(265,4,18),(266,4,18),(267,4,18),(268,4,18),(269,0,19),(270,0,19),(271,0,19),(272,1,19),(273,1,19),(274,1,19),(275,1,19),(276,1,19),(277,1,19),(278,1,19),(279,1,19),(280,1,19),(281,1,19),(282,1,19),(283,1,19),(284,1,19),(285,1,19),(286,1,19),(287,1,19),(288,1,19),(289,1,19),(290,1,19),(291,2,19),(292,2,19),(293,2,19),(294,2,19),(295,2,19),(296,2,19),(297,2,19),(298,2,19),(299,3,19),(300,3,19),(301,3,19),(302,3,19),(303,3,19),(304,3,19),(306,0,20),(307,0,20),(308,0,20),(309,0,20),(310,0,20),(311,0,20),(312,0,20),(313,0,20),(314,1,20),(315,1,20),(316,1,20),(317,1,20),(318,1,20),(319,1,20),(320,2,20),(321,2,20),(323,0,21),(324,0,21),(325,0,21),(326,0,21),(327,0,21),(328,0,21),(329,0,21),(330,0,21),(331,0,21),(332,0,21),(333,0,21),(334,0,21),(335,0,21),(336,1,21),(337,1,21),(338,1,21),(340,0,22),(341,0,22),(342,0,22),(343,1,22),(344,1,22),(345,1,22),(346,1,22),(347,1,22),(348,1,22),(349,2,22),(350,2,22),(351,0,23),(352,0,23),(353,0,23),(354,0,23),(355,0,23),(356,0,23),(357,0,23),(358,0,23),(359,0,23),(360,0,23),(361,0,23),(362,0,23),(363,0,23),(364,0,23),(365,0,23),(366,0,23),(367,1,23),(368,1,23),(369,1,23),(370,1,23),(371,1,23),(372,1,23),(373,1,23),(374,1,23);
/*!40000 ALTER TABLE `content_intellectually_stimulating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available_course_id` bigint(20) NOT NULL,
  `module_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_3h602uvck3d31wjve2do2vgx3` (`available_course_id`),
  KEY `FK_aqynit3d6qdc4gf49nmhy48ok` (`module_id`),
  CONSTRAINT `FK_3h602uvck3d31wjve2do2vgx3` FOREIGN KEY (`available_course_id`) REFERENCES `available_course` (`id`),
  CONSTRAINT `FK_aqynit3d6qdc4gf49nmhy48ok` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,2,1),(2,2,2),(3,1,2),(4,1,3),(5,2,3),(6,1,4),(7,1,5),(8,2,6),(9,1,7),(10,2,7),(11,1,8),(12,1,9),(13,2,10),(14,1,11),(15,2,11),(16,1,12),(17,2,12),(18,2,13),(19,2,14),(20,1,15),(21,1,16),(22,2,16),(23,1,17),(24,1,18),(25,2,19),(26,2,20),(27,2,21),(28,2,22),(29,1,23),(30,2,23),(31,1,24),(32,1,25),(33,1,26),(34,2,26),(35,1,27),(36,2,27),(37,2,28),(38,1,29),(39,1,30),(40,2,30),(41,1,31),(42,1,32),(43,1,33),(44,1,34);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback_on_written_work_useful`
--

DROP TABLE IF EXISTS `feedback_on_written_work_useful`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback_on_written_work_useful` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `response` int(11) DEFAULT NULL,
  `spf_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_nrcgaw1matpfh3nkk0q9mu8v1` (`spf_id`),
  CONSTRAINT `FK_nrcgaw1matpfh3nkk0q9mu8v1` FOREIGN KEY (`spf_id`) REFERENCES `student_preference_forms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=375 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_on_written_work_useful`
--

LOCK TABLES `feedback_on_written_work_useful` WRITE;
/*!40000 ALTER TABLE `feedback_on_written_work_useful` DISABLE KEYS */;
INSERT INTO `feedback_on_written_work_useful` VALUES (1,0,1),(2,0,1),(3,0,1),(4,0,1),(5,0,1),(6,0,1),(7,0,1),(8,0,1),(9,0,1),(10,0,1),(11,0,1),(12,0,1),(13,1,1),(14,1,1),(15,1,1),(16,2,1),(17,3,1),(18,3,1),(19,0,2),(20,0,2),(21,0,2),(22,1,2),(23,1,2),(24,1,2),(25,1,2),(26,2,2),(27,2,2),(28,0,3),(29,0,3),(30,1,3),(31,1,3),(32,1,3),(33,1,3),(34,1,3),(35,1,3),(36,1,3),(37,1,3),(38,1,4),(39,2,4),(41,0,5),(42,1,5),(43,1,5),(44,1,5),(45,1,5),(46,2,5),(47,2,5),(48,0,6),(49,0,6),(50,1,6),(51,2,6),(52,2,6),(53,2,6),(54,2,6),(55,2,6),(56,4,6),(57,4,6),(58,4,6),(59,0,7),(60,0,7),(61,0,7),(62,0,7),(63,0,7),(64,1,7),(65,1,7),(66,1,7),(67,1,7),(68,1,7),(69,1,7),(70,1,7),(71,1,7),(72,1,7),(73,0,8),(74,0,8),(75,1,8),(76,2,8),(77,3,8),(78,0,9),(79,0,9),(80,0,9),(81,0,9),(82,1,9),(83,2,9),(84,2,9),(85,3,9),(98,0,10),(99,0,10),(100,0,10),(101,0,10),(102,0,10),(103,0,10),(104,1,10),(105,1,10),(106,1,10),(107,1,10),(108,1,10),(109,1,10),(110,2,10),(113,0,11),(114,0,11),(115,0,11),(116,0,11),(117,0,11),(118,0,11),(119,1,11),(120,1,11),(121,2,11),(122,2,11),(123,3,11),(124,3,11),(129,0,12),(130,0,12),(131,1,12),(132,1,12),(133,1,12),(134,2,12),(135,2,12),(136,2,12),(137,2,12),(138,3,12),(139,4,12),(141,0,13),(142,0,13),(143,0,13),(144,0,13),(145,0,13),(146,0,13),(147,0,13),(148,0,13),(149,0,13),(150,0,13),(151,0,13),(152,0,13),(153,0,13),(154,0,13),(155,0,13),(156,1,13),(157,1,13),(158,1,13),(159,1,13),(160,1,13),(161,1,13),(162,1,13),(163,1,13),(164,1,13),(165,1,13),(166,1,13),(167,1,13),(168,1,13),(169,1,13),(170,1,13),(171,1,13),(172,2,13),(173,2,13),(174,2,13),(175,2,13),(176,2,13),(177,2,13),(178,2,13),(179,2,13),(180,2,13),(181,2,13),(182,3,13),(183,0,14),(184,0,14),(185,0,14),(186,0,14),(187,0,14),(188,0,14),(189,0,14),(190,1,14),(191,1,14),(192,1,14),(193,1,14),(194,1,14),(195,2,14),(196,2,14),(197,2,14),(198,2,14),(199,2,14),(200,2,14),(201,3,14),(202,3,14),(203,0,15),(204,0,15),(205,0,15),(206,0,15),(207,0,15),(208,0,15),(209,0,15),(210,0,15),(211,0,15),(212,0,15),(213,0,15),(214,1,15),(215,1,15),(216,1,15),(217,1,15),(218,1,15),(219,1,15),(220,1,15),(221,2,15),(222,2,15),(223,2,15),(224,2,15),(225,3,15),(227,0,17),(228,0,17),(229,0,17),(230,0,17),(231,0,17),(232,0,17),(233,0,17),(234,0,17),(235,1,17),(236,1,17),(237,3,17),(238,4,17),(239,1,18),(240,1,18),(241,1,18),(242,1,18),(243,2,18),(244,2,18),(245,2,18),(246,2,18),(247,2,18),(248,2,18),(249,2,18),(250,3,18),(251,3,18),(252,3,18),(253,4,18),(254,4,18),(269,0,19),(270,0,19),(271,0,19),(272,0,19),(273,0,19),(274,1,19),(275,1,19),(276,1,19),(277,2,19),(278,2,19),(279,2,19),(280,2,19),(281,2,19),(282,2,19),(283,2,19),(284,3,19),(285,0,20),(286,0,20),(287,0,20),(288,0,20),(289,0,20),(290,1,20),(291,1,20),(292,1,20),(293,1,20),(294,1,20),(295,1,20),(296,1,20),(297,1,20),(298,1,20),(299,2,20),(300,2,20),(323,0,21),(324,0,21),(325,0,21),(326,0,21),(327,0,21),(328,0,21),(329,0,21),(330,0,21),(331,0,21),(332,0,21),(333,1,21),(334,1,21),(335,1,21),(336,2,21),(337,2,21),(338,2,21),(340,0,22),(341,0,22),(342,0,22),(343,0,22),(344,0,22),(345,0,22),(346,0,22),(347,1,22),(348,1,22),(349,1,22),(350,3,22),(351,0,23),(352,0,23),(353,0,23),(354,0,23),(355,0,23),(356,0,23),(357,0,23),(358,0,23),(359,0,23),(360,0,23),(361,1,23),(362,1,23),(363,1,23),(364,1,23),(365,1,23),(366,1,23),(367,1,23),(368,1,23),(369,1,23),(370,1,23),(371,1,23),(372,1,23),(373,2,23),(374,3,23);
/*!40000 ALTER TABLE `feedback_on_written_work_useful` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fully_engaged_with_module`
--

DROP TABLE IF EXISTS `fully_engaged_with_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fully_engaged_with_module` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `response` int(11) DEFAULT NULL,
  `spf_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_flt8evxe0j4hd7he34oyiyqrv` (`spf_id`),
  CONSTRAINT `FK_flt8evxe0j4hd7he34oyiyqrv` FOREIGN KEY (`spf_id`) REFERENCES `student_preference_forms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=375 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fully_engaged_with_module`
--

LOCK TABLES `fully_engaged_with_module` WRITE;
/*!40000 ALTER TABLE `fully_engaged_with_module` DISABLE KEYS */;
INSERT INTO `fully_engaged_with_module` VALUES (1,0,1),(2,0,1),(3,0,1),(4,0,1),(5,0,1),(6,0,1),(7,0,1),(8,0,1),(9,0,1),(10,1,1),(11,1,1),(12,1,1),(13,1,1),(14,1,1),(15,1,1),(16,1,1),(17,3,1),(18,4,1),(19,0,2),(20,0,2),(21,1,2),(22,1,2),(23,1,2),(24,1,2),(25,1,2),(26,1,2),(27,2,2),(28,0,3),(29,1,3),(30,1,3),(31,1,3),(32,2,3),(33,2,3),(34,3,3),(35,3,3),(36,4,3),(37,4,3),(38,4,3),(39,2,4),(40,2,4),(41,0,5),(42,0,5),(43,0,5),(44,1,5),(45,1,5),(46,1,5),(47,1,5),(48,2,5),(49,0,6),(50,0,6),(51,0,6),(52,0,6),(53,0,6),(54,0,6),(55,0,6),(56,1,6),(57,1,6),(58,1,6),(59,1,6),(60,1,6),(61,1,6),(62,2,6),(63,2,6),(64,3,6),(65,3,6),(66,3,6),(67,3,6),(68,4,6),(69,4,6),(70,4,6),(71,0,7),(72,0,7),(73,0,7),(74,0,7),(75,0,7),(76,1,7),(77,1,7),(78,1,7),(79,1,7),(80,1,7),(81,1,7),(82,1,7),(83,2,7),(84,2,7),(85,0,8),(86,0,8),(87,1,8),(88,1,8),(89,4,8),(90,0,9),(91,0,9),(92,0,9),(93,0,9),(94,1,9),(95,1,9),(96,1,9),(97,1,9),(98,3,9),(99,4,9),(100,0,10),(101,0,10),(102,0,10),(103,0,10),(104,0,10),(105,0,10),(106,0,10),(107,1,10),(108,1,10),(109,1,10),(110,2,10),(111,2,10),(112,3,10),(113,0,11),(114,0,11),(115,0,11),(116,0,11),(117,0,11),(118,0,11),(119,0,11),(120,0,11),(121,1,11),(122,1,11),(123,1,11),(124,1,11),(125,1,11),(126,1,11),(127,3,11),(128,3,11),(129,0,12),(130,0,12),(131,0,12),(132,0,12),(133,0,12),(134,1,12),(135,1,12),(136,1,12),(137,1,12),(138,1,12),(139,3,12),(140,3,12),(141,0,13),(142,0,13),(143,0,13),(144,0,13),(145,0,13),(146,0,13),(147,0,13),(148,0,13),(149,0,13),(150,0,13),(151,0,13),(152,0,13),(153,0,13),(154,1,13),(155,1,13),(156,1,13),(157,1,13),(158,1,13),(159,1,13),(160,1,13),(161,1,13),(162,1,13),(163,1,13),(164,1,13),(165,1,13),(166,1,13),(167,1,13),(168,1,13),(169,1,13),(170,1,13),(171,1,13),(172,1,13),(173,1,13),(174,1,13),(175,2,13),(176,2,13),(177,2,13),(178,2,13),(179,2,13),(180,2,13),(181,4,13),(182,0,14),(183,0,14),(184,0,14),(185,0,14),(186,1,14),(187,1,14),(188,1,14),(189,1,14),(190,1,14),(191,1,14),(192,1,14),(193,1,14),(194,1,14),(195,1,14),(196,1,14),(197,1,14),(198,2,14),(199,2,14),(200,3,14),(201,3,14),(203,0,15),(204,0,15),(205,0,15),(206,1,15),(207,1,15),(208,1,15),(209,1,15),(210,1,15),(211,1,15),(212,1,15),(213,1,15),(214,1,15),(215,2,15),(216,2,15),(217,2,15),(218,2,15),(219,2,15),(220,2,15),(221,2,15),(222,2,15),(223,2,15),(224,3,15),(225,3,15),(226,4,15),(227,0,17),(228,0,17),(229,0,17),(230,0,17),(231,0,17),(232,0,17),(233,0,17),(234,0,17),(235,0,17),(236,0,17),(237,1,17),(238,4,17),(239,1,18),(240,1,18),(241,1,18),(242,1,18),(243,1,18),(244,1,18),(245,1,18),(246,1,18),(247,1,18),(248,1,18),(249,1,18),(250,1,18),(251,2,18),(252,2,18),(253,2,18),(254,2,18),(255,2,18),(256,2,18),(257,2,18),(258,2,18),(259,3,18),(260,3,18),(261,3,18),(262,3,18),(263,3,18),(264,3,18),(265,3,18),(266,3,18),(267,3,18),(269,0,19),(270,0,19),(271,1,19),(272,1,19),(273,1,19),(274,1,19),(275,1,19),(276,1,19),(277,1,19),(278,1,19),(279,1,19),(280,1,19),(281,1,19),(282,1,19),(283,1,19),(284,1,19),(285,1,19),(286,1,19),(287,1,19),(288,1,19),(289,2,19),(290,2,19),(291,2,19),(292,2,19),(293,2,19),(294,2,19),(295,2,19),(296,2,19),(297,2,19),(298,3,19),(299,3,19),(300,3,19),(301,3,19),(302,3,19),(303,3,19),(304,4,19),(306,0,20),(307,0,20),(308,0,20),(309,0,20),(310,0,20),(311,0,20),(312,0,20),(313,1,20),(314,1,20),(315,1,20),(316,1,20),(317,1,20),(318,1,20),(319,2,20),(320,2,20),(321,2,20),(323,0,21),(324,0,21),(325,0,21),(326,0,21),(327,0,21),(328,0,21),(329,0,21),(330,0,21),(331,1,21),(332,1,21),(333,1,21),(334,1,21),(335,1,21),(336,2,21),(337,2,21),(340,0,22),(341,0,22),(342,0,22),(343,0,22),(344,1,22),(345,1,22),(346,1,22),(347,1,22),(348,1,22),(349,1,22),(350,1,22),(351,0,23),(352,0,23),(353,0,23),(354,0,23),(355,0,23),(356,0,23),(357,0,23),(358,0,23),(359,0,23),(360,0,23),(361,0,23),(362,0,23),(363,1,23),(364,1,23),(365,1,23),(366,1,23),(367,1,23),(368,1,23),(369,1,23),(370,1,23),(371,1,23),(372,1,23),(373,1,23),(374,1,23);
/*!40000 ALTER TABLE `fully_engaged_with_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materials_resources_high_standard`
--

DROP TABLE IF EXISTS `materials_resources_high_standard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materials_resources_high_standard` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `response` int(11) DEFAULT NULL,
  `spf_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_k94kqnjdxt170bm8p04s3gm9d` (`spf_id`),
  CONSTRAINT `FK_k94kqnjdxt170bm8p04s3gm9d` FOREIGN KEY (`spf_id`) REFERENCES `student_preference_forms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=375 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materials_resources_high_standard`
--

LOCK TABLES `materials_resources_high_standard` WRITE;
/*!40000 ALTER TABLE `materials_resources_high_standard` DISABLE KEYS */;
INSERT INTO `materials_resources_high_standard` VALUES (1,0,1),(2,0,1),(3,0,1),(4,0,1),(5,0,1),(6,0,1),(7,0,1),(8,0,1),(9,0,1),(10,0,1),(11,0,1),(12,1,1),(13,1,1),(14,1,1),(15,1,1),(16,1,1),(17,1,1),(18,2,1),(19,0,2),(20,0,2),(21,0,2),(22,0,2),(23,0,2),(24,1,2),(25,1,2),(26,1,2),(27,2,2),(28,0,3),(29,0,3),(30,0,3),(31,0,3),(32,1,3),(33,1,3),(34,1,3),(35,1,3),(36,2,3),(37,2,3),(38,2,3),(39,1,4),(40,1,4),(41,0,5),(42,0,5),(43,1,5),(44,1,5),(45,1,5),(46,1,5),(47,2,5),(48,0,6),(49,0,6),(50,0,6),(51,0,6),(52,0,6),(53,0,6),(54,1,6),(55,1,6),(56,1,6),(57,1,6),(58,1,6),(59,1,6),(60,1,6),(61,1,6),(62,1,6),(63,2,6),(64,2,6),(65,2,6),(66,2,6),(67,2,6),(68,2,6),(69,3,6),(70,0,7),(71,0,7),(72,0,7),(73,0,7),(74,0,7),(75,1,7),(76,1,7),(77,1,7),(78,1,7),(79,1,7),(80,1,7),(81,1,7),(82,2,7),(83,0,8),(84,0,8),(85,1,8),(86,1,8),(87,1,8),(88,0,9),(89,0,9),(90,1,9),(91,1,9),(92,1,9),(93,2,9),(94,3,9),(95,3,9),(96,4,9),(97,4,9),(98,0,10),(99,0,10),(100,0,10),(101,0,10),(102,1,10),(103,1,10),(104,1,10),(105,1,10),(106,1,10),(107,1,10),(108,1,10),(109,2,10),(110,2,10),(113,0,11),(114,0,11),(115,0,11),(116,0,11),(117,0,11),(118,0,11),(119,0,11),(120,0,11),(121,0,11),(122,0,11),(123,1,11),(124,1,11),(125,1,11),(126,1,11),(127,2,11),(128,2,11),(129,0,12),(130,0,12),(131,0,12),(132,1,12),(133,2,12),(134,2,12),(135,3,12),(136,3,12),(137,3,12),(138,4,12),(139,4,12),(141,0,13),(142,0,13),(143,0,13),(144,0,13),(145,0,13),(146,0,13),(147,0,13),(148,0,13),(149,0,13),(150,0,13),(151,0,13),(152,0,13),(153,0,13),(154,0,13),(155,0,13),(156,0,13),(157,0,13),(158,0,13),(159,0,13),(160,0,13),(161,0,13),(162,1,13),(163,1,13),(164,1,13),(165,1,13),(166,1,13),(167,1,13),(168,1,13),(169,1,13),(170,1,13),(171,1,13),(172,1,13),(173,1,13),(174,1,13),(175,1,13),(176,1,13),(177,1,13),(178,1,13),(179,1,13),(180,1,13),(181,2,13),(182,3,13),(183,0,14),(184,0,14),(185,0,14),(186,0,14),(187,0,14),(188,0,14),(189,1,14),(190,1,14),(191,1,14),(192,1,14),(193,1,14),(194,1,14),(195,1,14),(196,1,14),(197,1,14),(198,2,14),(199,2,14),(200,2,14),(201,2,14),(202,2,14),(203,0,15),(204,0,15),(205,0,15),(206,0,15),(207,0,15),(208,0,15),(209,0,15),(210,0,15),(211,0,15),(212,0,15),(213,1,15),(214,1,15),(215,1,15),(216,1,15),(217,1,15),(218,1,15),(219,1,15),(220,2,15),(221,2,15),(222,2,15),(223,2,15),(224,2,15),(225,2,15),(226,3,15),(227,0,17),(228,0,17),(229,0,17),(230,0,17),(231,0,17),(232,0,17),(233,0,17),(234,0,17),(235,0,17),(236,1,17),(237,1,17),(238,4,17),(239,0,18),(240,1,18),(241,1,18),(242,1,18),(243,1,18),(244,1,18),(245,1,18),(246,1,18),(247,1,18),(248,1,18),(249,1,18),(250,2,18),(251,2,18),(252,2,18),(253,2,18),(254,2,18),(255,2,18),(256,3,18),(257,3,18),(258,3,18),(259,3,18),(260,3,18),(261,3,18),(262,3,18),(263,3,18),(264,4,18),(265,4,18),(266,4,18),(267,4,18),(268,4,18),(269,0,19),(270,0,19),(271,0,19),(272,0,19),(273,0,19),(274,0,19),(275,0,19),(276,0,19),(277,0,19),(278,0,19),(279,1,19),(280,1,19),(281,1,19),(282,1,19),(283,1,19),(284,1,19),(285,1,19),(286,1,19),(287,1,19),(288,1,19),(289,1,19),(290,1,19),(291,1,19),(292,1,19),(293,1,19),(294,2,19),(295,2,19),(296,2,19),(297,2,19),(298,2,19),(299,2,19),(300,2,19),(301,2,19),(302,2,19),(303,2,19),(304,3,19),(306,0,20),(307,0,20),(308,0,20),(309,0,20),(310,0,20),(311,0,20),(312,0,20),(313,0,20),(314,0,20),(315,1,20),(316,1,20),(317,1,20),(318,1,20),(319,1,20),(320,1,20),(321,2,20),(323,0,21),(324,0,21),(325,0,21),(326,0,21),(327,1,21),(328,1,21),(329,1,21),(330,1,21),(331,1,21),(332,1,21),(333,1,21),(334,2,21),(335,2,21),(336,2,21),(337,2,21),(338,3,21),(340,0,22),(341,0,22),(342,0,22),(343,0,22),(344,0,22),(345,0,22),(346,1,22),(347,1,22),(348,1,22),(349,2,22),(350,2,22),(351,0,23),(352,0,23),(353,0,23),(354,0,23),(355,0,23),(356,0,23),(357,0,23),(358,0,23),(359,0,23),(360,0,23),(361,0,23),(362,0,23),(363,0,23),(364,0,23),(365,0,23),(366,1,23),(367,1,23),(368,1,23),(369,1,23),(370,1,23),(371,1,23),(372,1,23),(373,1,23),(374,1,23);
/*!40000 ALTER TABLE `materials_resources_high_standard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `credits` int(11) DEFAULT NULL,
  `lecturer` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `module_code` varchar(255) DEFAULT NULL,
  `feedback_score` double DEFAULT NULL,
  `school_id` bigint(20) NOT NULL,
  `number_of_optional_prerequisits_needed` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `available_for_incoming_study_abroad` tinyint(1) NOT NULL,
  `discovery_module` tinyint(1) NOT NULL,
  `skills_discovery_module` tinyint(1) NOT NULL,
  `year` varchar(255) DEFAULT NULL,
  `module_catalogue_link` varchar(255) DEFAULT NULL,
  `cw_count` int(11) DEFAULT NULL,
  `exam_count` int(11) DEFAULT NULL,
  `objectives` longtext,
  `syllabus` longtext,
  `exam_type` varchar(255) DEFAULT NULL,
  `learning_outcomes` longtext,
  `skills_outcomes` longtext,
  `module_summary` longtext,
  PRIMARY KEY (`id`),
  KEY `FK_6m9rbob6bvbo195o31vgsqcq7` (`school_id`),
  CONSTRAINT `FK_6m9rbob6bvbo195o31vgsqcq7` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` VALUES (1,20,'Dr Marc de Kamps','Models and Simulation','COMP1345',1,1,0,1,1,0,0,'2013/2014','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201314&M=COMP-1345',20,80,'On completion of this module, students should be able to:\n- understand the relevance of simulation and modelling to Information Technology;\n- describe practical applications of simulation;\n- demonstrate awareness of the limitations of simulation in specific instances;\n- demonstrate awareness of the tools and techniques used in specific real-world simulation problems;\n- understand how to prepare problems for solution using simulation tools including basic modelling principles;\n- interpret the output provided by simulation tools in the solution of specific problems and perform basic sensitivity analysis.\n- demonstrate awareness of a range of optimisation tools and techniques. ','- Simulation tools: applications, inputs and outputs\n- Discrete event simulation and systems\n- Finite state machines\n- Cellular automata and other discrete models; with applications\n- Random numbers and random number generation\n- Statistics of distributions\n- Simulation applications and case studies in communication and other (e.g. transport) networks\n- Applications of difference equations\n- Validation and verification\n- Sensitivity analysis\n- Critique of selected research papers on modelling and computational simulation\n- Tools and techniques of optimisation and network flow','OpenBook',NULL,NULL,NULL),(2,20,'Dr Karim Djemame','Computer Systems','COMP1440',1,1,0,1,1,0,0,'2013/2014','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201314&M=COMP-1440',30,70,'On completion of this module, students should be able to understand:\n\n- how data and program instructions are represented on modern computer systems;\n- the von Neumann architecture;\n- the role of the operating system;\n- networking concepts, protocols, techniques and technologies;\n- Web technologies and techniques. \n','The von Neumann architecture and the role of the operating system, internal data representation, ALU, IO, memory, caching, disk storage and file systems, processes and CPU scheduling, buses, interconnects and interrupts, the fetch-execute cycle, mobile operating systems.\n\nNetworking: 4-layer approach, physical comms, cabling, wireless, LANs, MANs, WANs, ethernet protocol, networking devices.\nClient server model.\n\nWeb: HTTP, HTML, servers, browsers, Web-based applications.','OpenBook',NULL,NULL,NULL),(3,40,'Dr Andy Bulpitt','Core Programming','COMP1551',1,1,0,1,1,0,0,'2013/2014','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201314&M=COMP-1551',60,40,'On completion of this module, students should be able to ...\n\n-Analyse a problem and construct and test an algorithmic solution to the problem in pseudocode.\n\n-Implement, test, debug and verify pseudocode solutions in standard imperative programming languages. \n\n-Understand and use modular program construction, functional isolation, and recursion in solving larger algorithmic problems.\n\n-Understand differences between programming languages, and criteria for matching languages to problems.\n\n-Write and understand simple programs using languages from different language families.\n\n-Design an object-oriented solution to a problem and implement it in a suitable language.','Algorithmic Problem Solving:\nProblem-solving strategies; the role of algorithms for solving a problem; debugging strategies; use of pseudocode;\nFundamental Constructs:\nBasic syntax and semantics of a higher-level language; Variables, types, expressions, and assignment; Simple I/O; Conditional and iterative control structures; Functions and parameter passing; Structured decomposition; Using libraries\n\nData structures:\nRepresentation of numeric data; Range, precision, and rounding errors; Arrays; Representation of character data; Strings and string processing; Runtime storage management; Pointers and references; Linked structures; Stacks, Queues, Hash tables; Graphs and Trees; Strategies for choosing the right data structure.\n\nRecursion: \nThe concept of recursion; Simple recursive functions; \nObject Oriented programming:\nObject-oriented design; Encapsulation and information-hiding; Classes and subclasses; Inheritance; Polymorphism\n\nCompilers and interpreters\nIntegrated development environments\nStrategies and tools for testing\nDebugging tools\n\n','OpenBook',NULL,'Construct and test algorithmic solutions in pseudocode. \nImplement, test and debug solutions in standard imperative languages.',NULL),(4,20,'Dr Natasha Shakhlevich','Algorithms I','COMP1641',1,1,0,1,1,0,0,'2013/2014','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201314&M=COMP-1641',20,80,'On completion of this module, students should be able to:\n- understand the term \'model of computation\' and be familiar with a\nnumber of standard models\n- understand the theory of automata and grammars, and its applications\n- understand the basics computability, and demonstrate the existence\nof undecidable problems\n- understand the operation of iterative and recursive algorithms;\n- understand the importance of algorithm analysis in computing;\n-analyse the time complexity of several types of algorithms;\n- appreciate the importance of data structures and their implementation.','Formal languages and automata:\nRegular languages: finite automata (DFA, NFA), regular expressions,\nlinear grammars, equivalence of models, minimisation of DFA, pumping lemma for regular languages. \nContext-free languages: context-free grammars and normal forms, parsing and parse trees, ambiguity, push-down automata. Recursive languages: Turing machines, the Church-Turing thesis, recursive and recursively enumerable languages, the halting problem.\nAlgorithms and their analysis: Iterative algorithms, recursive algorithms, worst-case analysis of algorithms, big O, simple recurrence relations. Searching and sorting algorithms: sequential and binary search, selection sort, insertions sort, quicksort, mergesort, heapsort. Simple data structures: arrays, lists, stacks, queues, heaps, search trees and hash tables.','ClosedBook',NULL,NULL,NULL),(5,20,'Professor Kristina Vuskovic','Mathematics for Computing','COMP1740',1,1,0,1,1,0,0,'2013/2014','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201314&M=COMP-1740',20,80,'On completion of this module, students should be able to: understand and be proficient in applying some of the mathematical concepts and techniques that are fundamental to computing, and in particular those that fall within the areas of logic, set theory, functions and relations, combinatorics, discrete probability and graph theory. ','Semester 1: \nPropositional logic: propositions, connectives, truth tables, tautologies, contradictions; predicates, quantifiers; proof techniques (including mathematical induction). Set theory: sets, set operations, Venn diagrams, set equality, subsets, cardinality. Relations: relations on a set, inverse relations, equivalence relations, orders. Functions: domain and range, inverse functions, composition of functions, properties of functions. Number systems: decimal, binary and hexadecimal. \n\nSemester 2:\nCombinatorics: multiplication principle, addition principle, permutations with unlimited repetition, combinations with unlimited repetition. Discrete probability theory: experiment, sample space, event, finite probability space, probability of an event, equiprobable spaces, conditional probability, independent events. Graph theory: graph models, graph isomorphism, vertex degrees, paths and cycles, Euler\'s theorem (Euler cycles and paths), bipartite graphs, trees. ','ClosedBook',NULL,NULL,NULL),(6,20,'Dr Nick Efford','Web Development','COMP1745',1,1,0,1,1,0,0,'2013/2014','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201314&M=COMP-1745',50,50,'On completion of this module, students should be able to:\n- Understand markup language concepts\n- Comprehend the need to keep presentation separate from content\n- Use modern versions of HTML and CSS to create accessible web pages\n- Appreciate how copyright, data protection and equality legislation affect web site development\n- Implement basic client-side behaviour and interaction using JavaScript\n- Understand how web servers handle requests from clients\n- Use a web framework to implement dynamic, database-driven web applications','- Architecture of the World-Wide Web; browsers and servers; HTTP\n- Web development methods; requirements capture; design tools\n- Markup languages; HTML5 elements; embedded media; validation; making markup accessible\n- Separation of content from presentation ; CSS3 concepts: inheritance, selectors, flow, the box model; styling of HTML elements; page layout; CSS frameworks; handling print media\n- Testing websites; tools for accessing accessibility; WCAG guidelines\n- Intellectual property issues; data protection; implications of the Equality Act 2010\n- The Document Object Model; implementing behaviour using jQuery and raw JavaScript\n- Generating pages from templates; handling form data; building content using databases and SQL queries; developing with a server-side framework\n- Session management; use of cookies; security issues\n\n','OpenBook','Ability to develop the client and server sides of simple web applications.','Modern, standard-based website development.',NULL),(7,20,'Ms Jill Duggleby','Professional Development','COMP1940',1,1,0,1,1,0,0,'2013/2014','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201314&M=COMP-1940',0,100,'On completion of this module, students should be able to:\n- demonstrate that they have given a presentation and have engaged in group work;\n- reflect upon groupwork which was carried out to undertake a relevant task;\n- undertake a literature search and prepare a properly formatted bibliography;\n- present reasoned arguments based on appropriate source materials, using appropriate citation;\n- write a structured technical report addressing the computing audience;\n- make recommendations on professional issues based on appropriate criteria;\n- audit, evaluate and critically reflect upon strengths and weaknesses in knowledge, skills and abilities;\n- understand the meaning of academic integrity and how to avoid plagiarism;\n- discuss ethical, legal, social and professional issues in IT.\n- describe the key characteristics of IT projects\n- understand the roles of key people in IT projects\n- apply and reflect upon the use of an appropriate project management tool\n- understand the importance of risk analysis and deploy appropriate techniques to the management of risk and quality\n- understand the causes and consequences of project failure and under-performance\n- undertake and reflect upon the management of a mini-IT project ','- Group working skills, presentation skills, time management, academic writing skills, including avoiding plagiarism. Reflecting on feedback.\n- Personal Development Planning. Presenting yourself.\n- Ethical issues relating to: data kept on computer systems (including Web-based systems); privacy and security policies; professional issues (such as, for example, contract working); academic issues (including referencing and group working).\n- Characteristics of IT projects; project roles and responsibility; project management theories, techniques, processes and tools; risk and risk analysis, project success and failure; quality management and evaluation. ','OpenBook',NULL,NULL,NULL),(8,20,'Dr Mark Walkley','Networks and Scalable Architectures','COMP2444',1,1,0,2,1,0,0,'2014/2015','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201415&M=COMP-2444',40,60,'On completion of this module, students should be able to understand: \n- the importance of scalability in all aspects of modern computing\n- concepts and terminology for multi-core systems\n- constructing scalable networked applications using a three-tier architecture for client-server systems\n- constructing scalable distributed parallel applications using MPI\n- concepts, technology and scalability for modern operating systems ','Network programming\n- Scalability as an application property\n- Network communication protocols and standards\n- 3-tier network architecture\n- Multithreaded programming\n- Implementation of scalable client-server models\nDistributed parallel programming\n- Scalability as an algorithmic property\n- MPI and scalable parallel programming\n- Communication models: send/receive\n- Blocking and non-blocking communication\n- Parallel algorithms\n- Parallel efficiency and strong/weak scalability \nOperating systems\n- Scalability as a system property\n- Virtual memory\n- File systems\n- Virtualisation\n- Examples of standard, modern operating systems\n- HPC operating systems','OpenBook','Scalable architectures in modern computer systems. \nMulti-core programming in shared-memory or distributed-memory.\nNetwork and multithreaded programming.\nDistributed parallel programming and parallel algorithm design.\nModern computer architectures and management.','Programming in C (covered in Level 1 Core Programming)\nProgramming in Java (covered in Semester 1 of this course)',NULL),(9,20,'Dr Olaf Beyersdorff','Information Management and Security','COMP2446',1,1,0,2,1,0,0,'2014/2015','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201415&M=COMP-2446',20,80,'On completion of this module, students should be able to:\n- Identify the main functions of a database management system (DBMS)\n- Describe the architecture of a relational database system\n- Produce a relational schema from an entity-relationship conceptual model.\n- Explain the distinction between procedural and declarative queries and \nthe meaning and significance of relational completeness.\n- Express simple queries using relational algebra and relational calculus.\n- Determine functional dependencies and use inference rules to generate functional dependencies.\n- Use SQL to create, maintain and manipulate data in a relational database.\n- Determine what normal form a table is in, and explain the advantages and disadvantages of normalization.\n- Explain the importance of information systems not based on the relational model.\n\n- Describe the types of threats to data and information systems\n- Describe the purpose of cryptography and list ways it is used in data communications\n- Understand global principles for the design of efficient and secure cryptographic algorithms \n- Understand mathematical foundations of symmetric and public-key cryptography \n- Discuss complexity-theoretic assumptions for the design of secure and efficient cryptosystems\n- Discuss the different cryptographic primitives and the work function of each\n- Identify the common type of crytographic attacks and describe how the attack can occur\n- Describe how cryptographic primitives can be employed to design complex cryptographic protocols \n- Describe the architecture for public and private key cryptography and how PKI supports network security\n','- History of, motivation for, and functions of database systems\n- The relational data model\n- Mapping conceptual schemas to relational schemas.\n- Foundations of procedural and declarative query languages: \nrelational algebra, relational calculus, relational completeness,\nconnections with other logical formalisms including 1st order logic.\n- Functional dependencies, Armstrong\'s axioms and other inference rules\n- Normal forms: 1NF, 2NF, 3NF, BCNF and selected higher normal forms\n- SQL including data definition, queries, and integrity constraints\n- Relational database design including applications of normalization\n- Non-relational / NoSQL databases (e.g. OO, graph, triplestore)\n\n-Information Assurance Concepts (Confidentiality, Integrity, Availability)\n- Cryptography: history, main goals, security requirements\n- Types of attacks to cryptosystems\n- Symmetric cryptosystems: DES, AES, operation modes\n- Mathematical background from number theory and finite fields\n- Efficient algorithms for arithmetic and primality\n- Public-key cryptography: Diffie-Hellman key exchange, RSA, ElGamal\n- Digital signatures\n- Hash functions, their use and security\n- Cryptographic protocols: bit commitment, electronic voting\n- Network security, network attack types \n- Use of cryptography for network security','ClosedBook',NULL,NULL,NULL),(10,20,'Dr Andy Bulpitt','Networking and IT Management','COMP2449',1,1,0,2,1,0,0,'2014/2015','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201415&M=COMP-2449',40,60,'On completion of this module, students should be able to ...\n1.	Explain and apply modern networking principles\n2.	Theoretically design small networks and IP schemes\n3.	Build and test networks involving hubs, switches and routers\n4.	Build and test a network with a wireless access point connected\n5.	Explain the properties of network cabling\n6.	Demonstrate an understanding of the TCP/IP Protocol suite\n7.	Evaluate various operating systems and applications and recommend a particular operating system and application to satisfy given needs of an organization.\n8.	Install, modify & update the configuration of at least one current operating system, application, sever and client services.\n9.	Discuss the importance of system and application configuration and maintenance for an organization.\n10.	Identify and explain the responsibilities associated with server administration and management.\n','Review of TCP/IP protocol stack\nCabling and signaling\nInterface configuration, naming, and addressing Connecting and configuring computers, hubs, switches, and routers into an Ethernet \nLAN.\nRouting and switching functions\nVLAN and inter-VLAN routing\nConnecting and configuring access points in a wireless LAN.\nReliable and secure access to networked resources Packet tracing Wide \nArea Network protocol configuration, e.g. Frame Relay Network testing \nNetwork scalability Network performance Network troubleshooting \nOperating system support. User and privileged mode Hypervisor and \nVirtualization Reliable and secure access to networked resources: security policy, authentication, encryption, VPNs Setting up and securing a web server and WWW services.\nServer administration and management;\nContent management; Content deployment (file system planning and structure); User and group management Backup management Security management Disaster recovery Resource management; Automation management (automatic job scheduling); System support User support and education\n\n','OpenBook',NULL,'Problem Solving\nCollaboration\nPractical application of skills',NULL),(11,20,'Dr Nick Efford','Software Engineering','COMP2541',1,1,0,2,1,0,0,'2014/2015','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201415&M=COMP-2541',100,0,'On completion of this module, students should able to:\n- Understand the software lifecycle;\n- Understand the differences between the software development processes commonly used in industry;\n- Select a development process appropriate to a given problem;\n- Work effectively as a team on all stages of a medium-sized software project, following a suitable development process;\n- Capture requirements effectively and use a range of analysis and modelling techniques to produce a design;\n- Implement a design using a suitable object-oriented programming language;\n- Use appropriate tools to plan and manage project tasks, control source code, track issues, run tests and deploy the end product.\n','- Software lifecycle;\n- Waterfall and spiral models for software development;\n- Unified Process and its variants;\n- Agile methods: Scrum, XP, others;\n- Techniques for requirements capture;\n- Object-oriented analysis & design;\n- Use cases;\n- UML modelling with class diagrams and collaboration diagrams;\n- Architectural challenges;\n- Design patterns;\n- Use of software components;\n- Testing and test-driven development;\n- Human factors in software development;\n- Project management tools: version control, build automation, issue tracking;\n- Packaging and deployment.','NoExam','Ability to plan and carry out software projects in a team.','Software development.\n\n',NULL),(12,20,'Dr Nick Efford','Graphical User Interfaces','COMP2542',1,1,0,2,1,0,0,'2014/2015','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201415&M=COMP-2542',40,60,'On the completion of the module, students should be able to:\n- appreciate the role of usability and user experience in interactive computer applications;\n- understand the foundations of designing interactive systems;\n- understand the structure of user interface toolkits;\n- implement graphical applications using a user interface toolkit;\n- design for effective human-computer interaction;\n- develop low-fidelity prototypes for human-computer interfaces;\n- implement simple applications for a mobile device.\n','- Human constraints on user interactions;\n- Physical & cognitive limits of human sensory apparatus;\n- Interactive systems architectures; usability and user experience;\n- Understanding problem space;\n- Generic design principles;\n- Cognitive, social and emotional aspects.\n\n- Computer representation of images;\n- Colour representations;\n- Desktop graphics;\n- Event loops and user interaction;\n- Desktop user interface toolkits (e.g. Qt);\n- Mobile interface toolkits (e.g. Android / iOS).\n\n- Process of interaction design;\n- Techniques for establishing requirements, design, prototyping and evaluation of interfaces.\n- A major piece of design work as practical.','OpenBook','Ability to design and implement effective graphical user interfaces.\n\n','- Software development;\n- Design.',NULL),(13,20,'Dr Kevin McEvoy','Strategy and Security','COMP2645',1,1,0,2,1,0,0,'2014/2015','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201415&M=COMP-2645',20,80,'On completion of this module, students should be able to:\n- understand the role of different stakeholders;\n- understand the concept of strategy and the relationship between IT strategy and organisational strategy;\n- understand how performance against strategy may be monitored and evaluated;\n- understand current issues in information security;\n- demonstrate awareness of the techniques for developing an information security management system;\n- identify potential system vulnerabilities and the impact of a security breach on business continuity;\n- understand social, ethical and legal implications of information storage. ','- The role of strategy: Purpose; development; challenges; execution.\n- Organisational structure: stakeholders.\n- Success metrics: impact factors, business improvement, benefits realisation.\n- Information Security Management: Incentives; techniques for building an information security management system; information assets; risk assessment; threats and vulnerabilities; controls; implications for business continuity; documentation; standards; cryptography.\n- Social, ethical and legal issues: compliance; data preservation; data retention policies; privacy; intellectual property rights; international law.','OpenBook',NULL,NULL,NULL),(14,20,'Ms Jill Duggleby','Business Applications','COMP2646',1,1,0,2,1,0,0,'2014/2015','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201415&M=COMP-2646',20,80,'On completion of this module, students should be able to:\n- compare and contrast the IT infrastructure of a range of organisations;\n- demonstrate awareness of a range of business functions and the contributions they make to the enterprise;\n- identify applications that support a range of business processes and explain how those applications contribute to efficiency and effectiveness;\n- understand the role of IT in supporting a range of modern working practices;\n- understand the strategic importance of the types of data held by modern organisations;\n- understand the importance of data security and be aware of typical policies and procedures;\n- understand the implications for business applications of current trends in distributed systems. \n','Organisations: SME, national and multi-national organisations; commercial, not-for-profit and public sector.\nInfrastructure: Internal and external networking; desktop; portable; remote access; IT support; training.\nBusiness functions: Financial and management accounting; marketing; production; human resource management.\nInformation quality.\nApplications: Customer Relationship Management (CRM); Enterprise Resource Planning (ERP); Supply Chain Management (SCM); SAP; e-Business, distributed applications.\nImplications of trends in distributed systems.','OpenBook',NULL,NULL,NULL),(15,20,'Dr Haiko Muller','Algorithms II','COMP2941',1,1,0,2,1,0,0,'2014/2015','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201415&M=COMP-2941',20,80,'On completion of this module, students should be able to:\n- Understand how to compute with vectors and matrices;\n- Appreciate the role of numerical computation in computer science;\n- Choose a computational algorithm appropriately, accounting for issues of accuracy, reliabilty and efficiency;\n- Understand how to assess/measure the error in a numerical algorithm and be familar with how such errors are controlled;\n- Understand the fundamental techniques for the deisgn of efficient algorithms;\n- Demonstrate how these algorithms are analysed;\n- Understand several advanced data structures, their efficient implementation and applications;\n- Understand how these algorithms and data structures relate to the central practical problems of modern computer science.','Semester 1:\nVectors and matrices: vectors, vector operations (sum, scalar multiplication, difference, dot product, norm), matrices, matrix operations (sum, scalar multiplication, difference, multiplication), identity matrix; inverse of a matrix.\nApproximation: converting a real-world problem, via a mathematical model, to a form which can be understood by a computer; discretising a continuous model; measuring, analysing and controlling approximation errors; balancing accuracy and efficiency. Static systems: simple iterative methods for solving nonlinear scalar equations; direct and iterative methods for solving linear systems of equations.\nEvolving systems: differentiation as rate of change and as the limit of a gradient (including derivatives of simple functions); initial value ordinary differential equations, simple methods for initial value problems.\nSemester 2: \nPrinciples of algorithm design:\nRepresentations of graphs: adjacency list, adjacency matrix. Depth- and breadth-first search traversals, topological sort, shortest-paths algorithms (Dijkstra\'s and Floyd\'s algorithm), transitive closure (Floyd\'s algorithm), minimum spanning tree (Prim\'s and Kruskal\'s algorithms, union-find data structure). Algorithmic strategies: greedy algorithm, divide-and-conquer, dynamic programming. Recurrence equations, Master theorem. Strassen\'s algorithm for matrix multiplication. Binary search trees, balanced search trees','ClosedBook',NULL,NULL,NULL),(16,10,'Dr Nick Efford','Mobile Application Development','COMP3222',1.0059,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3222',100,0,'On completion of this module, students should be able to ...\n-Understand how mobile hardware constrains application development, and what architectural and computational compromises are typically made.\n-Design and structure both the user interface and the underlying software structure for a mobile application based on sound software engineering principles.\n-Develop small to medium mobile applications on at least one mobile platform / OS.\n-Understand how to interact with the sensor-rich hardware of typical mobile devices.\n-Understand past, present and future mobile application development.','- Principles of mobile development, including constraints imposed by battery power, touch interfaces, portability and always-on networking, operating system limitations and design strategies.\n- Application and modification of standard software methodologies, user interface design and object-oriented programming for mobile development.\n- Design constraints and choices for touch screens and small interface sizes. Limitations of GUI-only development. Application of design patterns in mobile development. Client-server applications on mobile devices. Separation of GUI development from internal processing. Portability of code between mobile and stationary platforms.\n- Hardware sensors on mobile applications and implications for design and development. Networking limitations of mobile devices. Ad hoc network construction and library solutions to network development. Cross-platform networking constraints. Low-level networking on mobile devices.\n-Access to remote data and services, both proprietary and open-source. Vertical integration with enterprise software and with free services including geo-location. Development cycles and business models for mobile development.\n-Security restrictions and strategies for mobile devices, including hardware, software and firmware attacks.\n-Defense strategies for mobile developers and device designers.\n\n','NoExam','On completion of the year/programme students should have provided evidence of being able to:\n-understand and demonstrate coherent and detailed subject knowledge and professional competencies some of which will be informed by recent research/scholarship in the discipline;\n-deploy accurately standard techniques of analysis and enquiry within the discipline;\n-demonstrate a conceptual understanding which enables the development and sustaining of an argument;',NULL,'Understand the constraints of developing for mobile devices from hardware, interface and networking. Design and structure the user interface and underlying software structure for a mobile application. Integrate input from hardware sensors and networked data and services. Implement mobile applications on a given platform.'),(17,10,'Dr Andy Bulpitt','Computer Vision','COMP3300',0.442215,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3300',20,80,'On completion of this module, students should be able to:\n- know the major applications in which computer vision is and has been active;\n- understand the principal ideas and techniques of computer vision;\n- understand a selection of these techniques;\n- appreciate the problems and solutions adopted in some of its main application areas;\n- apply the theoretical knowledge gained in the module to the design of vision systems for solving specific problems. ','- Segmentation\n- Shape description and modelling\n- Geometry of projection and co-ordinate transformations\n- Binocular and motion stereo\n- Surface reconstruction\n- Recognising and tracking objects\n- Recognising activities and events\n- Image registration\nApplications of computer vision.','OpenBook',NULL,NULL,NULL),(18,20,'Dr Brandon Bennett','Artificial Intelligence','COMP2240',1,1,0,2,1,0,0,'2014/2015','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-2240',10,90,'On completion of this module, students should be able to:\n- understand the scope and nature of artificial intelligence in the discipline of computing;\n- understand the fundamental ideas and techniques within the main approaches to artificial intelligence;\n- implement a simple intelligent system by applying suitable theoretical concepts from artificial intelligence;\n- appreciate how complex real world processes (e.g. biology, language, speech, reasoning, etc.) can be analysed and modelled, and of the role of these techniques in developing artificial intelligence applications;\n- understand the main concepts of text mining and corpus processing;\n- understand the main concepts in signal processing and their relevance to areas such as speech and image processing and bioinformatics;\n- understand the main concepts in statistical learning and optimization;\n- understand the main concepts of knowledge representation and inference. \n','- Overview of AI, its scope, history, and prospects. Search techniques and problem solving (informed and uninformed search, heuristic search, genetic algorithms, game playing). Uncertainty (applications of probability, Bayesian networks, Hidden Markov Models). Basic concepts of machine learning.\n- Corpora; web-as-corpus text capture, cleansing, tokenisation; type-token frequency distributions, Zipf\'s law; stemming, morphology, collocations, concordances; Information Retrieval.\n- Signal Acquisition, Linear transforms, Filtering, Temporal normalisation/alignment, Spatial normalisation, Selected applications.\n- Topics in statistical learning.\n- Knowledge Representation and Reasoning; First order predicate calculus; inference (including resolution); representing knowledge in logic.','OpenBook',NULL,NULL,NULL),(19,10,'Owen Johnson','Business Issues in Computing','COMP3441',1,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3441',20,80,'On completion of this module, students should be able to ...\n\n-	Describe the broad range of emerging information technologies which support innovation in organisations and business.\n-	Identify the business benefits of information systems, understand the challenges in realising business benefits and be able to apply benefits management techniques. \n-	Make recommendations for business strategies and policies which exploit the potential of computing to transform organisations and business processes.\n-	Identify solutions to legal, social, ethical and financial issues. \n-	Describe current business issues in information systems and information technology and be able to apply appropriate techniques for governance, risk assessment and management.','Information Technology (IT) and innovation, the relationship between computing and business process improvement. Business strategy and innovation, using Information Systems (IS) for competitive advantage and strategic alignment. Sources of business benefit, benefits management, benefits realisation, success and failure in software project management. Strategic planning for IS/IT, IS strategy, portfolio management approaches. Issues and solutions, the emerging landscape, technology development and adoption, market forces in IT, understanding and managing technology risks. Legal, social and ethical issues, IS/IT governance, the ITIL and COBIT frameworks.','ClosedBook','Application of techniques including benefits realisation, strategic thinking, management approaches as they relate to computing, infrastructure management. Problem solving based on identification and analysis of issues and assessment of candidate solutions.','IT Management. IS Strategy. Technology led innovation. Benefits management.','This module is designed to equip level three students with an understanding of the business issues in computing. It will cover a broad range of skills and approaches relevant to the successful management of information technology.'),(20,10,'Dr Roy Ruddle','Usability Design','COMP3442',1,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3442',20,80,'On completion of this module, students should be able to:\n\n- understand ethical issues from end-users\' perspective\n- design and prototype user interfaces\n- take account of the needs of a diverse user population\n- choose appropriate methods for usability evaluation ','Ethics: codes of practice and key organisations (e.g., BCS, ACM and IEEE); relevance to user interface design. Design fundamentals (e.g., errors, colour, feedback, human memory, motor skills, accessibility) and practice (e.g., guidelines, standards, and cross-platform delivery). Low and high fidelity approaches to prototyping. Expert and user evaluation methods.\n\n','OpenBook',NULL,NULL,NULL),(21,10,'Dr Vania Dimitrova','Knowledge Enriched Information Systems','COMP3725',1,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3725',20,80,'On completion of this module, students should be able to:\n- develop an appreciation of the basics of widely used knowledge representation and reasoning techniques and how these can be applied to enrich information systems;\n- understand the main principles and techniques used in semantic web applications.','1. Ontological modelling\n- core material: capturing and representing knowledge (which includes standard languages, such as RDF and OWL; )\n- applications: medical ontologies, topographic ontologies, modelling business processes; possible use of ROO tool for building ontologies\n\n2. Data integration and interoperability\n- core material: interoperability and data integration via ontologies applications.\n\n3. Data linking and querying \n- core material: representing and reasoning about semantic data; linked data principles and Web of data\n\n4. Multiple perspectives and contexts\n- core material: semantic augmentation; modelling context; personalisation and multi-perspective interaction with data.\n\n5. Practical examples of knowledge-enriched information systems.\n- core material: Practical examples of knowledge-enriched information systems based on research projects and industrial collaboration.','OpenBook','1. Familiarity with knowledge enriched web systems and the main technologies used in such systems.\n2. Practical experience of applying ontology engineering methodology in a specific context.\n3. Experience of using modelling tools.\n4. Understanding of the role knowledge enrichment can play in information systems.','Knowledge modelling and coding;\nUse of innovative tools;\nData query with SPARQL.',NULL),(22,20,'Dr Lydia Lau','Information Systems and Databases','COMP2448',1,1,0,2,1,0,0,'2014/2015','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-2448',60,40,'On completion of this module, students should be able to ...\n- Adopt systems thinking in analysing problem situations and in formulating solutions\n- Develop information systems architecture for an information driven solution\n- Explain the rationale for, and role of, information modelling techniques\n- Perform conceptual schema design, using an established methodology and notation\n- Transform conceptual schema into relational models\n- Describe relational database architecture and the functions of a database management system (DBMS)\n- Use SQL to create, maintain, and manipulate data in a relational database\n- Demonstrate understanding of non-relational models\n- Understand data mining principles and techniques\n- Apply practical methodology and toolkit for data mining projects. ','-	System thinking\n-	Soft System methodology\n-	Design for Integration – architectures\n-	Architectures framework (such as TOGAF) and examples of information systems architectures\n-	Role of information modelling in information systems design\n-	Introduction to advanced modelling issues\n-	Relationship between Entity-Relational modelling and UML\n-	Overview of database technologies and historical development\n-	Relational algebra, and Codd’s relational data model\n-	Relational database management systems (DBMS)\n-	Normal forms and normalisation\n-	SQL and database management\n-	Non-relational databases (such as graph database)\n-	Introduction to Data mining inputs and outputs\n-	The WEKA toolkit for applied Data Mining\n-	CRISP-DM: Cross Industry Standard Process for Data Mining\n-	Supervised Machine Learning of data classifiers\n-	Unsupervised Machine Learning of data clusters and associations\n-	Text data mining and Information Extraction','ClosedBook',NULL,NULL,NULL),(23,10,'Dr Roy Ruddle','Information Visualization','COMP3736',0.6552533333333332,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3736',20,80,'On completion of this module, students should be able to:\n- Understand the use of visualization in decision-making and knowledge discovery;\n- Understand core technical concepts including data and processing models;\n- Understand salient features of human perceptual and cognitive processing;\n- Design new visual representations appropriate for a given task;\n- Modify and apply visualization tools to obtain insight about a given dataset.\n','- Overview of visualization, scivis, infovis, visual analytics; big data;\n- Visual design, examples of both appropriate and inappropriate representation;\n- Perception: the eye, light and colour, pre-attentive processing, gestalt phenomena;\n- Cognition, tasks and knowledge, semiotics of representations;\n- Different types of data, and the link between data, task, and representation;\n- Processing technologies, in particular the pipeline model, and web-based approaches;\n- Visualization as process, with schneiderman\'s mantra and the need for interaction;\n- Visual analytics for finding patterns in huge datasets;\n- Practical visualization using tools such as excel, nodexl (http:// nodexl.codeplex.com; open source) and tableau (http://www.tableausoftware.com/data-visualization-software; provided through the tableau for teaching program.','OpenBook','On completion of the year/programme students should have provided evidence of being able to:\n-understand and demonstrate coherent and detailed subject knowledge and professional competencies some of which will be informed by recent research/scholarship in the discipline;\n-deploy accurately standard techniques of analysis and enquiry within the discipline;\n-demonstrate a conceptual understanding which enables the development and sustaining of an argument;\n-describe and comment on particular aspects of recent research and/or scholarship;\n-appreciate the uncertainty, ambiguity and limitations of knowledge in the discipline;\n-make appropriate use of scholarly reviews and primary sources;\n-apply their knowledge and understanding in order to initiate and carry out an extended piece of work or project;\n\n',NULL,'Covers the principles of visualisation in decision making and knowledge discovery. You will learn how to design new visual representations and how to modify and apply existing tools to gain new insight about data.'),(24,10,'Dr Marc de Kamps','Biological and Bio-Inspired Computation','COMP3750',0.6933433333333332,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3750',20,80,'On completion of this module, students should be able to:\n- understand how natural computing and conventional AI can complement each other;\n- understand algorithms that are based on cooperative behaviour of distributed systems with no, or little central control;\n- understand, design and apply simple genetic algorithms;\n- understand the relation between artificial neural networks and statistical learning;\n- understand how the fields of artificial neural networks and computational and cognitive neuroscience inform each other. ','Examples of cooperative phenomena in nature; concepts such as emergence, self-organisation and embodiment; genetic algorithms; algorithms for swarm intelligence; biological neural networks; various artificial neural networks and their application (e.g. clustering, dimensionality reduction); models in computational and cognitive neuroscience; models of biological computation in computational/cognitive neuroscience and/or bioinformatics.','OpenBook',NULL,NULL,NULL),(25,10,'Dr John Stell','Knowledge Representation','COMP3760',0.6631549999999999,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3760',20,80,'On completion of this module, students should be able to:\n- analyse informal descriptions of simple problems in terms of a formal representation language;\n- use an automated reasoning software tool to compute inferences from logical representations;\n- understand the basic principles of automated reasoning and appreciate its power and its limitations.','Logical foundations of knowledge representation including key properties of formal systems (such as soundness, completeness, expressiveness and tractability).\nUse of theorem proving techniques (e.g., Tableaux) and tools (e.g., Prover9).\nRepresenting and reasoning about time and actions (e.g., STRIPS, situation calculus, tense logic). \nQualitative knowledge representation.\nIntroduction to non-monotonic reasoning.','OpenBook',NULL,'Formal model building.',NULL),(26,10,'Dr Lydia Lau','User Adaptive Intelligent Systems','COMP3771',0.98193,1,1,3,1,0,0,'2015/2016',NULL,20,80,'On completion of this module, students should be able to:\n- apply human-computer interaction methodology to identify user needs, draw requirements, design, and evaluate user-adaptive systems;\n- identify most common techniques for user modelling and adaptation and apply them in practical areas;\n- implement one or more recommender system techniques in a practical application;\n- reason about the significance of user-adaptive systems and directions the field is going to develop.\n','- Adaptable and adaptive systems;\n- General Schema of User-adaptive Systems;\n- Basics for User Modelling;\n- User Profiling (implicit and explicit methods);\n- Stereotypes (construction and use);\n- Modelling user knowledge and beliefs, affect and context;\n- Adaptive Content Presentation - static and dynamic;\n- Recommender Systems - item-item and user-user collaborative filtering, content-based approach, and hybrid approaches;\n- Issues of scalability, diversity, potentials and drawbacks;\n- Making prediction about the User;\n- Evaluation of adaptive systems;\n- Trends.\n\n','OpenBook','Experience and understanding of techniques for user modelling and their application to build user adaptive intelligent systems\n\n',NULL,NULL),(27,10,'Dr Eric Atwell','Data Mining and Text Analytics','COMP3776',1.5508366666666666,1,0,3,1,0,0,'2015/2016','',20,80,'On completion of this module, students should be able to:\n- understand theory and terminology of empirical modelling of natural language;\n- understand and use algorithms, resources and techniques for implementing and evaluating data mining and text analytics systems;\n- demonstrate familiarity with some of the main text mining and text analytics application areas;\n- appreciate why unrestricted natural language processing is still a major research task.\n','Introduction to data and text theory and terminology. Tools and techniques for data-mining and text processing, focusing on applied and corpus-based problems such as data classification by Machine Learning classifiers, collocation and co-occurrence discovery and text analytics. Open-source and commercial text mining and text analytics toolkits. Web-based text analytics.\n\nCase studies of current commercial applications in text mining, Beyond English, Arabic data, machine translation, information retrieval, information extraction, text classification. Current research in text analytics at Leeds University\n\n','OpenBook',NULL,NULL,NULL),(28,10,'Dr Raymond Kwan','Decision Modelling','COMP3778',1,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3778',20,80,'On completion of this module, students should be able to ...\n- understand the importance and relevance of decision models for decision making in a variety of IT contexts;\n- demonstrate awareness of a range of optimisation tools and techniques; \n- demonstrate awareness of specific search techniques;\n- critically evaluate different decision models given a decision setting\n- prepare problems for solution using optimisation tools;\n- build an appropriate mathematical model of a decision problem,\n- analyze the output of the model for specific problems and perform sensitivity analysis; ','- Tools and techniques: for Operational Research; for Scheduling; for Network traffic;\n- Formulation of Linear Programming models; Properties and assumptions of LP models; Graphical solutions; Minimization problems\n- Search: Depth-first; breadth-first; heuristics; Tabu search; simulated annealing\n- Decision Trees\n- Sensitivity Analysis\n- Case studies: scheduling; stock control ','OpenBook',NULL,NULL,NULL),(29,10,'Dr Hamish Carr','Computer Graphics','COMP3811',0.6511916666666666,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3811',50,50,'On completion of this module, students should be able to:\n- Appreciate the physical limitations on computational representation and display of visual scenes in three dimensions, including hardware-accelerated graphics;\n- Understand the use of 3D modelling to represent visual scenes, including geometric approximations of surfaces in general and including triangulated approximations;\n- Understand the application of raytracing and projective rendering to produce visual scenes in three dimensions;\n- Understand how users navigate in and interact with three dimensional graphics;\n- Develop the ability to implement graphical applications using industry standard hardware-accelerated graphical libraries (e.g. OpenGL).','- Physics & biology of vision, colour, display technology, human-computer interaction, navigation, object manipulation and evaluation;\n- 3D interface design and interaction.\n- 3D geometric modelling and transformation;\n- Orthographic and perspective transformations;\n- Homogeneous coordinates;\n- Triangulated surfaces, higher-order surfaces (e.g. Bézier surfaces).\n- Scene construction and representation;\n- Scene graphs and animation hierarchies;\n- Basic computer animation techniques.\n- Phong lighting and rendering equations;\n- Surface parametrization and textured surfaces;\n- Phong & Gouraud shading;\n- Shadows and reflections.\n- Rendering methodology: raytracing, projective rendering, projective rendering pipeline;\n- Standard libraries for real-time rendering (e.g., OpenGL).\n- Advanced topic(s) drawn from areas such as: Physics modelling, collision detection, height fields & terrain;\n- Advanced graphical modelling techniques;\n- Modern graphics hardware, shader languages and GPGPU programming;\n- Virtual reality;\n- Giga-pixel displays.','ClosedBook','On completion of the year/programme students should have provided evidence of being able to:\n-understand and demonstrate coherent and detailed subject knowledge and professional competencies some of which will be informed by recent research/scholarship in the discipline;\n-deploy accurately standard techniques of analysis and enquiry within the discipline;\n-demonstrate a conceptual understanding which enables the development and sustaining of an argument;\n-appreciate the uncertainty, ambiguity and limitations of knowledge in the discipline;',NULL,'3D computer graphics are at the heart of computer games and special effects in film, but are also widely used in sciences, medicine and engineering. This module covers the core concepts behind 3D computer graphics, including rasterisation, raytracing, hardware-accelerated projective rendering, geometric modelling of curves and surfaces, surface texturing, hierarchical animation and introductory shader-based rendering. In addition, this module surveys standard modelling tasks such as terrain and plant rendering, physics modelling, collision detection, higher-order surfaces, shadows and reflections, culminating in students building a small real-time 3D application from scratch to showcase their ability.'),(30,10,'Dr Karim Djemame','Distributed Systems','COMP3900',0.5013333333333334,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3900',30,70,'On completion of this module, students should be able to:\n- understand advanced technologies used in typical Internet Information Systems;\n- understand current middleware technologies such as remote method invocation and Web services;\n- implement simple distributed applications using standard middleware tools;\n- comprehend the role played by naming, resource discovery and synchronisation services in a range of distributed systems;\n- discuss future directions in distributed systems. ','- Definitions of distributed systems\n- Applications challenges\n- Systems challenges\n- the client-server model\n- Middleware\n- The N-tier model\n- Message oriented communication\n- Remote database access\n- XML based technologies; \n- Implementation of middleware using a distributed object model; \n- Remote method invocation technologies; Service Oriented Architectures\n- Web services\n- Naming, directory, and discovery services, Synchronisation in distributed systems\n- Transactions\n- Open computing environments\n- Emerging distributed systems\n- Grid computing\n- Cloud computing.','OpenBook',NULL,NULL,NULL),(31,10,'Dr Natasha Shakhlevich','Combinatorial Optimisation','COMP3910',0.777895,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3910',20,80,'On completion of this module, students should be able to:\n- Understand the power and scope of linear programming and integer linear programming;\n- Use other approaches to solve combinatorial optimisation problems and have an appreciation of the interaction between the solution process and problem formulation.\n','- Linear programming: simplex method, duality and the dual simplex method;\n- Integer Linear Programming: modelling of combinatorial problems and logical conditions;\n- Solution by branch and bound;\n- Network flow problems;\n- Other solution approaches: construction and improvement heuristics.','ClosedBook','On completion of the year/programme students should have provided evidence of being able to:\n-understand and demonstrate coherent and detailed subject knowledge and professional competencies some of which will be informed by recent research/scholarship in the discipline;\n-deploy accurately standard techniques of analysis and enquiry within the discipline;\n-demonstrate a conceptual understanding which enables the development and sustaining of an argument;\n-describe and comment on particular aspects of recent research and/or scholarship;\n-appreciate the uncertainty, ambiguity and limitations of knowledge in the discipline;\n-make appropriate use of scholarly reviews and primary sources;\n-apply their knowledge and understanding in order to initiate and carry out an extended piece of work or project;',NULL,'Understand how problems can be modelled as integer linear programs and solved using simplex based branch-and-bound. Appreciate the role of problem-specific heuristics in improving solution algorithms.'),(32,10,'Dr. David Head','Parallel Scientific Computing','COMP3920',1.1980366666666669,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3920',20,80,'On completion of this module, students should be able to:\n- show awareness of the range of parallel computer architectures and programming paradigms;\n- understand the role of parallel scientific computing and the importance of reliability, efficiency and accuracy;\n- understand how to design algorithms to make efficient use of parallel architectures and how to predict and measure the efficiency and scalability of an implementation;\n- write portable parallel programs using the message passing system MPI;\n- understand what partial differential equations are and how they can be approximated by a discrete system which can be solved on a computer;\n- implement simple parallel Scientific Computing algorithms in an efficient manner. ','Basics of parallel computing: distributed and shared memory architectures; multicore processors; programming paradigms; MPI; threads.\n\nParallel programming with MPI: basic computational procedures; types of task and data partitioning; load-balancing; communications and synchronization.\n\nParallel efficiency: speed-up; efficiency; scalability; isoefficiency; performance analysis.\n\nIntroduction to parallel scientific computing: reliability, efficiency and accuracy of computational methods; the role of parallel computation; partial differential equations (PDEs) and their use in the modelling of physical systems.\n\nParallel Scientific Computing problems: boundary value problems; initial-boundary value problems; finite difference approximation; domain decomposition; block and strip partitioning; explicit and implicit time-stepping; red-black ordering.\n\nAdvanced parallel scientific computing: complex geometry; unstructured data; graph partitioning; sparse matrices and data structures; direct versus iterative solvers; multigrid and parallel implementations.\n\n','ClosedBook',NULL,NULL,NULL),(33,10,'Dr Haiko Muller','Graph Algorithms and Complexity Theory','COMP3940',1.3888888888888888,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3940',20,80,'On completion of this module students should be able to:\n- recognise real-world problems that can be modelled by graph problems considered in this module;\n- understand and be able to use these algorithms;\n- demonstrate how such algorithms are analysed;\n- construct reductions to prove NP-hardness;','Network flows (Ford-Fulkerson Labelling Algorithm, Max Flow Min Cut Theorem)\nMatchings (matchings in bipartite graphs, matchings in general graphs, Edmonds’ Blossom Algorithm, matchings and coverings, Hall’s Theorem, König-Egervary Theorem)\nComplexity Theory (the classes P and NP, NP-completeness, polynomial-time many-one reductions, proofs of NP-completeness of independent set, vertex cover and clique problems) ','ClosedBook','On completion of the year/programme students should have provided evidence of being able to:\n-understand and demonstrate coherent and detailed subject knowledge and professional competencies some of which will be informed by recent research/scholarship in the discipline;\n-deploy accurately standard techniques of analysis and enquiry within the discipline;\n-demonstrate a conceptual understanding which enables the development and sustaining of an argument;\n-appreciate the uncertainty, ambiguity and limitations of knowledge in the discipline\n\n',NULL,'A graph is a mathematical object defined by a set of vertices and a set of edges that connect these vertices. Many problems arising in diverse areas such as transportation, telecommunication, molecular biology, industrial engineering, etc. can be modeled by graphs. These problems then reduce to optimizing some graph parameter. Since the objects we are dealing with are finite, we can envision an algorithm that would list all the possibilities and go through them to find the desired optima. The problem with such an algorithm is its complexity, it grows exponentially with the input size. Even with relatively small input graph it would take the computer centuries to find the answer. The graphs used to model problems in say bioinformatics, or the problems related to the internet, are enormous in size, stressing the importance for the algorithms to be extremely efficient. To construct efficient algorithms one needs to exploit the mathematical understanding of the underlying objects.In this module we introduce a wide range of graph problems, classify them with respect to their computational complexity, and demonstrate how these methods extend to other computational problems.\n'),(34,10,'Prof David Duke','Functional Programming','COMP3941',1,1,0,3,1,0,0,'2015/2016','http://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=201516&M=COMP-3941',50,50,'This module examines the paradigm of programming with (pure) functions in software development, focussing on the Haskell language and its implementation technology. Similar concepts underpin other modern languages, including ML, Scala, JavaScript and Clojure. Indeed, concepts from functional programming are now found in C++. Functional programmers are in demand by industry: Haskell for example has been adopted by major companies (e.g. Facebook, and a number of banks) and is used by a range of specialist software developers in applications from web development to secure languages for programming embedded devices. In research, functional languages are the major laboratories for innovation in programming technologies including type theory, parallelism, software correctness, and security.\n','Students will understand the foundations of functional programming, and be able to write programs using basic to intermediate features of the Haskell language. The level of foundational knowledge will allow students to transfer insight into functional paradigm to programming in related languages. They will also have further exposure to the tool ecosystem that is essential to software development practice.','OpenBook','The module will: \n*	review the formal foundations of functional programming;\n*	equip students with an understanding of the major concepts in modern functional programming, including inductive data types, equational definitions, higher-order functions and higher-order types;\n*	introduce students to advanced features specific to the Haskell language, including the monad, applicative, and traversable type classes, and support for parallel programming;\n*	provide practical experience of software development in a pure functional language.\n\n',NULL,'*	foundations: the lambda calculus, redexes, evaluation order*	basic concepts: functional abstractions and applications*	simple and compound types*	user-defined inductive data types and type classes*	function definition*	higher-order functions*	higher-order types and Haskell type classes including Monad and Applicative*	tools for Haskell development including modules and Cabal*	relationship with other functional languages, for example Scala, Clojure, F#.');
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prerequisite`
--

DROP TABLE IF EXISTS `prerequisite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prerequisite` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `module_id` bigint(20) NOT NULL,
  `prereqmodule_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_5dytp37cc0hx800m04x5nqe43` (`module_id`),
  KEY `FK_p2t91c0k865xf9jps4cx6p1wn` (`prereqmodule_id`),
  CONSTRAINT `FK_5dytp37cc0hx800m04x5nqe43` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`),
  CONSTRAINT `FK_p2t91c0k865xf9jps4cx6p1wn` FOREIGN KEY (`prereqmodule_id`) REFERENCES `module` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prerequisite`
--

LOCK TABLES `prerequisite` WRITE;
/*!40000 ALTER TABLE `prerequisite` DISABLE KEYS */;
INSERT INTO `prerequisite` VALUES (1,'Compulsory',8,2),(2,'Compulsory',8,3),(3,'Compulsory',9,3),(4,'Compulsory',10,2),(5,'Compulsory',10,3),(6,'Compulsory',11,3),(7,'Compulsory',12,3),(8,'Compulsory',15,3),(9,'Compulsory',15,4),(10,'Compulsory',15,5),(11,'Compulsory',16,12),(12,'Compulsory',17,18),(13,'Compulsory',21,22),(14,'Compulsory',23,12),(15,'Compulsory',24,3),(16,'Compulsory',24,15),(17,'Compulsory',25,18),(18,'Optional',26,22),(19,'Optional',26,9),(20,'Compulsory',27,3),(21,'Compulsory',28,1),(22,'Compulsory',29,12),(23,'Compulsory',29,15),(24,'Optional',30,10),(25,'Optional',30,8),(26,'Compulsory',30,3),(27,'Compulsory',31,15),(28,'Compulsory',32,15),(29,'Compulsory',33,15);
/*!40000 ALTER TABLE `prerequisite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `satisfied_with_module_quality`
--

DROP TABLE IF EXISTS `satisfied_with_module_quality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `satisfied_with_module_quality` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `response` int(11) DEFAULT NULL,
  `spf_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7ulkc5sfp9udqn00ibb8mtakn` (`spf_id`),
  CONSTRAINT `FK_7ulkc5sfp9udqn00ibb8mtakn` FOREIGN KEY (`spf_id`) REFERENCES `student_preference_forms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=375 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `satisfied_with_module_quality`
--

LOCK TABLES `satisfied_with_module_quality` WRITE;
/*!40000 ALTER TABLE `satisfied_with_module_quality` DISABLE KEYS */;
INSERT INTO `satisfied_with_module_quality` VALUES (1,0,1),(2,0,1),(3,0,1),(4,0,1),(5,0,1),(6,0,1),(7,0,1),(8,0,1),(9,0,1),(10,0,1),(11,0,1),(12,1,1),(13,1,1),(14,1,1),(15,1,1),(16,1,1),(17,1,1),(18,1,1),(19,0,2),(20,0,2),(21,0,2),(22,0,2),(23,0,2),(24,0,2),(25,1,2),(26,1,2),(27,1,2),(28,0,3),(29,0,3),(30,0,3),(31,1,3),(32,1,3),(33,1,3),(34,1,3),(35,1,3),(36,2,3),(37,3,3),(38,1,4),(39,2,4),(40,0,5),(41,0,5),(42,0,5),(43,0,5),(44,1,5),(45,1,5),(46,1,5),(47,1,5),(48,0,6),(49,0,6),(50,0,6),(51,0,6),(52,1,6),(53,1,6),(54,1,6),(55,1,6),(56,1,6),(57,1,6),(58,1,6),(59,1,6),(60,1,6),(61,2,6),(62,2,6),(63,2,6),(64,2,6),(65,3,6),(66,3,6),(67,3,6),(68,4,6),(69,0,7),(70,0,7),(71,0,7),(72,0,7),(73,1,7),(74,1,7),(75,1,7),(76,1,7),(77,1,7),(78,1,7),(79,1,7),(80,1,7),(81,1,7),(82,2,7),(83,0,8),(84,0,8),(85,1,8),(86,1,8),(87,1,8),(88,0,8),(89,0,8),(90,0,9),(91,0,9),(92,0,9),(93,1,9),(94,1,9),(95,1,9),(96,1,9),(97,3,9),(98,0,10),(99,0,10),(100,0,10),(101,0,10),(102,0,10),(103,1,10),(104,1,10),(105,1,10),(106,1,10),(107,1,10),(108,1,10),(109,1,10),(110,2,10),(111,0,11),(112,0,11),(113,0,11),(114,0,11),(115,0,11),(116,0,11),(117,0,11),(118,0,11),(119,0,11),(120,0,11),(121,0,11),(122,0,11),(123,1,11),(124,1,11),(125,1,11),(126,2,11),(127,0,12),(128,0,12),(129,0,12),(130,0,12),(131,0,12),(132,0,12),(133,0,12),(134,1,12),(135,1,12),(136,1,12),(137,2,12),(138,3,12),(139,0,13),(140,0,13),(141,0,13),(142,0,13),(143,0,13),(144,0,13),(145,0,13),(146,0,13),(147,0,13),(148,0,13),(149,0,13),(150,0,13),(151,0,13),(152,0,13),(153,0,13),(154,0,13),(155,0,13),(156,0,13),(157,0,13),(158,0,13),(159,0,13),(160,1,13),(161,1,13),(162,1,13),(163,1,13),(164,1,13),(165,1,13),(166,1,13),(167,1,13),(168,1,13),(169,1,13),(170,1,13),(171,1,13),(172,1,13),(173,1,13),(174,1,13),(175,1,13),(176,1,13),(177,1,13),(178,1,13),(179,2,13),(180,2,13),(181,0,14),(182,0,14),(183,0,14),(184,0,14),(185,0,14),(186,0,14),(187,0,14),(188,0,14),(189,0,14),(190,1,14),(191,1,14),(192,1,14),(193,1,14),(194,1,14),(195,1,14),(196,1,14),(197,1,14),(198,1,14),(199,2,14),(200,2,14),(203,0,15),(204,0,15),(205,0,15),(206,0,15),(207,0,15),(208,1,15),(209,1,15),(210,1,15),(211,1,15),(212,1,15),(213,1,15),(214,1,15),(215,1,15),(216,1,15),(217,1,15),(218,1,15),(219,1,15),(220,2,15),(221,2,15),(222,2,15),(223,2,15),(224,3,15),(225,3,15),(226,0,17),(227,0,17),(228,0,17),(229,0,17),(230,0,17),(231,0,17),(232,0,17),(233,0,17),(234,0,17),(235,0,17),(236,1,17),(237,4,17),(239,0,18),(240,1,18),(241,1,18),(242,1,18),(243,1,18),(244,1,18),(245,1,18),(246,1,18),(247,1,18),(248,1,18),(249,1,18),(250,1,18),(251,1,18),(252,2,18),(253,2,18),(254,2,18),(255,2,18),(256,2,18),(257,2,18),(258,2,18),(259,2,18),(260,3,18),(261,3,18),(262,3,18),(263,3,18),(264,3,18),(265,3,18),(266,3,18),(267,4,18),(268,4,18),(269,0,19),(270,0,19),(271,1,19),(272,1,19),(273,1,19),(274,1,19),(275,1,19),(276,1,19),(277,1,19),(278,1,19),(279,1,19),(280,1,19),(281,1,19),(282,1,19),(283,1,19),(284,1,19),(285,1,19),(286,1,19),(287,1,19),(288,1,19),(289,1,19),(290,1,19),(291,1,19),(292,1,19),(293,1,19),(294,1,19),(295,1,19),(296,2,19),(297,2,19),(298,2,19),(299,2,19),(300,3,19),(301,3,19),(302,3,19),(303,3,19),(304,3,19),(306,0,20),(307,0,20),(308,0,20),(309,0,20),(310,0,20),(311,0,20),(312,0,20),(313,0,20),(314,0,20),(315,0,20),(316,1,20),(317,1,20),(318,1,20),(319,1,20),(320,1,20),(321,1,20),(323,0,21),(324,0,21),(325,0,21),(326,0,21),(327,0,21),(328,0,21),(329,0,21),(330,0,21),(331,0,21),(332,1,21),(333,1,21),(334,1,21),(335,1,21),(336,2,21),(337,2,21),(338,0,21),(339,0,21),(340,0,22),(341,0,22),(342,0,22),(343,0,22),(344,1,22),(345,1,22),(346,1,22),(347,1,22),(348,1,22),(349,1,22),(350,1,22),(351,0,23),(352,0,23),(353,0,23),(354,0,23),(355,0,23),(356,0,23),(357,0,23),(358,0,23),(359,0,23),(360,0,23),(361,0,23),(362,0,23),(363,0,23),(364,0,23),(365,0,23),(366,0,23),(367,1,23),(368,1,23),(369,1,23),(370,1,23),(371,1,23),(372,1,23),(373,1,23),(374,1,23);
/*!40000 ALTER TABLE `satisfied_with_module_quality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school`
--

DROP TABLE IF EXISTS `school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `university_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_aobyovv87d1ebifwairna61sd` (`university_id`),
  CONSTRAINT `FK_aobyovv87d1ebifwairna61sd` FOREIGN KEY (`university_id`) REFERENCES `university` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school`
--

LOCK TABLES `school` WRITE;
/*!40000 ALTER TABLE `school` DISABLE KEYS */;
INSERT INTO `school` VALUES (1,'School of Computing',1);
/*!40000 ALTER TABLE `school` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_made_subject_interesting`
--

DROP TABLE IF EXISTS `staff_made_subject_interesting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_made_subject_interesting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `response` int(11) DEFAULT NULL,
  `spf_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8d364pkon6v6l3npsauthdxo1` (`spf_id`),
  CONSTRAINT `FK_8d364pkon6v6l3npsauthdxo1` FOREIGN KEY (`spf_id`) REFERENCES `student_preference_forms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=375 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_made_subject_interesting`
--

LOCK TABLES `staff_made_subject_interesting` WRITE;
/*!40000 ALTER TABLE `staff_made_subject_interesting` DISABLE KEYS */;
INSERT INTO `staff_made_subject_interesting` VALUES (1,0,1),(2,0,1),(3,0,1),(4,0,1),(5,0,1),(6,0,1),(7,0,1),(8,0,1),(9,0,1),(10,0,1),(11,1,1),(12,1,1),(13,1,1),(14,1,1),(15,1,1),(16,1,1),(17,1,1),(18,2,1),(19,0,2),(20,0,2),(21,1,2),(22,1,2),(23,1,2),(24,1,2),(25,1,2),(26,2,2),(27,2,2),(28,0,3),(29,1,3),(30,1,3),(31,1,3),(32,2,3),(33,2,3),(34,3,3),(35,3,3),(36,3,3),(37,3,3),(38,4,3),(39,1,4),(40,2,4),(41,0,5),(42,0,5),(43,0,5),(44,0,5),(45,0,5),(46,1,5),(47,1,5),(48,1,5),(49,0,6),(50,0,6),(51,0,6),(52,0,6),(53,0,6),(54,1,6),(55,1,6),(56,1,6),(57,1,6),(58,1,6),(59,1,6),(60,2,6),(61,2,6),(62,2,6),(63,2,6),(64,3,6),(65,3,6),(66,3,6),(67,3,6),(68,3,6),(69,3,6),(70,4,6),(71,0,7),(72,0,7),(73,0,7),(74,0,7),(75,0,7),(76,0,7),(77,1,7),(78,1,7),(79,1,7),(80,1,7),(81,1,7),(82,2,7),(83,2,7),(84,3,7),(85,0,8),(86,0,8),(87,1,8),(88,1,8),(89,2,8),(90,0,9),(91,0,9),(92,0,9),(93,0,9),(94,0,9),(95,1,9),(96,1,9),(97,1,9),(98,2,9),(99,3,9),(100,0,10),(101,0,10),(102,0,10),(103,0,10),(104,0,10),(105,0,10),(106,0,10),(107,1,10),(108,1,10),(109,1,10),(110,1,10),(111,2,10),(112,3,10),(113,0,11),(114,0,11),(115,0,11),(116,0,11),(117,0,11),(118,0,11),(119,0,11),(120,0,11),(121,0,11),(122,0,11),(123,0,11),(124,1,11),(125,1,11),(126,1,11),(127,1,11),(128,2,11),(129,0,12),(130,0,12),(131,0,12),(132,0,12),(133,0,12),(134,1,12),(135,1,12),(136,1,12),(137,1,12),(138,1,12),(139,1,12),(140,1,12),(141,0,13),(142,0,13),(143,0,13),(144,0,13),(145,0,13),(146,0,13),(147,0,13),(148,0,13),(149,0,13),(150,0,13),(151,0,13),(152,0,13),(153,0,13),(154,0,13),(155,0,13),(156,0,13),(157,0,13),(158,0,13),(159,0,13),(160,0,13),(161,0,13),(162,0,13),(163,0,13),(164,0,13),(165,0,13),(166,0,13),(167,0,13),(168,0,13),(169,0,13),(170,0,13),(171,0,13),(172,0,13),(173,0,13),(174,0,13),(175,1,13),(176,1,13),(177,1,13),(178,1,13),(179,1,13),(180,1,13),(181,1,13),(182,2,13),(183,0,14),(184,0,14),(185,0,14),(186,0,14),(187,0,14),(188,0,14),(189,0,14),(190,0,14),(191,1,14),(192,1,14),(193,1,14),(194,1,14),(195,1,14),(196,1,14),(197,1,14),(198,1,14),(199,1,14),(200,1,14),(201,1,14),(202,2,14),(203,0,15),(204,0,15),(205,0,15),(206,0,15),(207,0,15),(208,0,15),(209,0,15),(210,0,15),(211,0,15),(212,0,15),(213,1,15),(214,1,15),(215,1,15),(216,1,15),(217,1,15),(218,1,15),(219,1,15),(220,1,15),(221,2,15),(222,2,15),(223,2,15),(224,2,15),(225,3,15),(226,3,15),(227,0,17),(228,0,17),(229,0,17),(230,0,17),(231,0,17),(232,0,17),(233,0,17),(234,0,17),(235,0,17),(236,0,17),(237,0,17),(238,4,17),(239,0,18),(240,0,18),(241,1,18),(242,1,18),(243,1,18),(244,1,18),(245,1,18),(246,1,18),(247,1,18),(248,1,18),(249,1,18),(250,1,18),(251,1,18),(252,1,18),(253,1,18),(254,2,18),(255,2,18),(256,2,18),(257,2,18),(258,2,18),(259,2,18),(260,2,18),(261,2,18),(262,2,18),(263,3,18),(264,3,18),(265,3,18),(266,3,18),(267,4,18),(268,4,18),(269,0,19),(270,0,19),(271,0,19),(272,0,19),(273,0,19),(274,1,19),(275,1,19),(276,1,19),(277,1,19),(278,1,19),(279,1,19),(280,1,19),(281,1,19),(282,1,19),(283,1,19),(284,1,19),(285,1,19),(286,1,19),(287,1,19),(288,1,19),(289,1,19),(290,1,19),(291,1,19),(292,1,19),(293,2,19),(294,2,19),(295,2,19),(296,2,19),(297,2,19),(298,2,19),(299,2,19),(300,2,19),(301,2,19),(302,2,19),(303,3,19),(304,3,19),(306,0,20),(307,0,20),(308,0,20),(309,0,20),(310,0,20),(311,0,20),(312,0,20),(313,0,20),(314,0,20),(315,0,20),(316,0,20),(317,1,20),(318,1,20),(319,1,20),(320,1,20),(321,2,20),(323,0,21),(324,0,21),(325,0,21),(326,0,21),(327,0,21),(328,0,21),(329,0,21),(330,0,21),(331,0,21),(332,0,21),(333,0,21),(334,0,21),(335,0,21),(336,1,21),(337,1,21),(340,0,22),(341,0,22),(342,0,22),(343,0,22),(344,0,22),(345,0,22),(346,0,22),(347,1,22),(348,1,22),(349,1,22),(350,1,22),(351,0,23),(352,0,23),(353,0,23),(354,0,23),(355,0,23),(356,0,23),(357,0,23),(358,0,23),(359,0,23),(360,0,23),(361,0,23),(362,0,23),(363,0,23),(364,0,23),(365,0,23),(366,0,23),(367,0,23),(368,1,23),(369,1,23),(370,1,23),(371,1,23),(372,1,23),(373,1,23),(374,1,23);
/*!40000 ALTER TABLE `staff_made_subject_interesting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_were_enthusiastic`
--

DROP TABLE IF EXISTS `staff_were_enthusiastic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_were_enthusiastic` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `response` int(11) DEFAULT NULL,
  `spf_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_92nhnn1mqqfe9820l134klcdr` (`spf_id`),
  CONSTRAINT `FK_92nhnn1mqqfe9820l134klcdr` FOREIGN KEY (`spf_id`) REFERENCES `student_preference_forms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=375 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_were_enthusiastic`
--

LOCK TABLES `staff_were_enthusiastic` WRITE;
/*!40000 ALTER TABLE `staff_were_enthusiastic` DISABLE KEYS */;
INSERT INTO `staff_were_enthusiastic` VALUES (1,0,1),(2,0,1),(3,0,1),(4,0,1),(5,0,1),(6,0,1),(7,0,1),(8,0,1),(9,0,1),(10,0,1),(11,0,1),(12,0,1),(13,0,1),(14,0,1),(15,0,1),(16,0,1),(17,1,1),(18,1,1),(19,0,2),(20,0,2),(21,0,2),(22,1,2),(23,1,2),(24,1,2),(25,1,2),(26,1,2),(27,2,2),(28,0,3),(29,0,3),(30,0,3),(31,0,3),(32,1,3),(33,1,3),(34,1,3),(35,1,3),(36,2,3),(37,2,3),(38,2,3),(39,1,4),(40,1,4),(41,0,5),(42,0,5),(43,0,5),(44,0,5),(45,0,5),(46,0,5),(47,0,5),(48,1,5),(49,0,6),(50,0,6),(51,0,6),(52,0,6),(53,0,6),(54,0,6),(55,0,6),(56,0,6),(57,0,6),(58,0,6),(59,0,6),(60,0,6),(61,1,6),(62,1,6),(63,1,6),(64,1,6),(65,1,6),(66,1,6),(67,1,6),(68,1,6),(69,1,6),(70,2,6),(71,0,7),(72,0,7),(73,0,7),(74,0,7),(75,0,7),(76,0,7),(77,0,7),(78,1,7),(79,1,7),(80,1,7),(81,1,7),(82,1,7),(83,1,7),(84,1,7),(85,0,8),(86,0,8),(87,0,8),(88,1,8),(89,2,8),(90,0,9),(91,0,9),(92,0,9),(93,0,9),(94,0,9),(95,0,9),(96,0,9),(97,1,9),(98,1,9),(99,2,9),(100,0,10),(101,0,10),(102,0,10),(103,0,10),(104,0,10),(105,0,10),(106,0,10),(107,0,10),(108,0,10),(109,1,10),(110,1,10),(111,1,10),(112,2,10),(113,0,11),(114,0,11),(115,0,11),(116,0,11),(117,0,11),(118,0,11),(119,0,11),(120,0,11),(121,0,11),(122,0,11),(123,0,11),(124,0,11),(125,0,11),(126,0,11),(127,0,11),(128,1,11),(129,0,12),(130,0,12),(131,0,12),(132,0,12),(133,0,12),(134,0,12),(135,0,12),(136,0,12),(137,0,12),(138,0,12),(139,0,12),(140,1,12),(141,0,13),(142,0,13),(143,0,13),(144,0,13),(145,0,13),(146,0,13),(147,0,13),(148,0,13),(149,0,13),(150,0,13),(151,0,13),(152,0,13),(153,0,13),(154,0,13),(155,0,13),(156,0,13),(157,0,13),(158,0,13),(159,0,13),(160,0,13),(161,0,13),(162,0,13),(163,0,13),(164,0,13),(165,0,13),(166,0,13),(167,0,13),(168,0,13),(169,0,13),(170,0,13),(171,0,13),(172,0,13),(173,0,13),(174,0,13),(175,0,13),(176,0,13),(177,0,13),(178,1,13),(179,1,13),(180,1,13),(181,1,13),(182,1,13),(183,0,14),(184,0,14),(185,0,14),(186,0,14),(187,0,14),(188,0,14),(189,0,14),(190,0,14),(191,0,14),(192,0,14),(193,0,14),(194,0,14),(195,0,14),(196,1,14),(197,1,14),(198,1,14),(199,1,14),(200,1,14),(201,2,14),(202,2,14),(203,0,15),(204,0,15),(205,0,15),(206,0,15),(207,0,15),(208,0,15),(209,0,15),(210,0,15),(211,0,15),(212,0,15),(213,0,15),(214,0,15),(215,0,15),(216,1,15),(217,1,15),(218,1,15),(219,1,15),(220,1,15),(221,1,15),(222,1,15),(223,1,15),(224,2,15),(225,3,15),(226,4,15),(227,0,17),(228,0,17),(229,0,17),(230,0,17),(231,0,17),(232,0,17),(233,0,17),(234,0,17),(235,0,17),(236,0,17),(237,0,17),(238,4,17),(239,0,18),(240,0,18),(241,0,18),(242,0,18),(243,0,18),(244,0,18),(245,0,18),(246,0,18),(247,0,18),(248,0,18),(249,0,18),(250,0,18),(251,0,18),(252,0,18),(253,0,18),(254,0,18),(255,1,18),(256,1,18),(257,1,18),(258,1,18),(259,1,18),(260,1,18),(261,1,18),(262,1,18),(263,1,18),(264,2,18),(265,3,18),(266,3,18),(267,4,18),(268,4,18),(269,0,19),(270,0,19),(271,0,19),(272,0,19),(273,0,19),(274,0,19),(275,0,19),(276,0,19),(277,0,19),(278,0,19),(279,0,19),(280,0,19),(281,0,19),(282,0,19),(283,0,19),(284,1,19),(285,1,19),(286,1,19),(287,1,19),(288,1,19),(289,1,19),(290,1,19),(291,1,19),(292,1,19),(293,1,19),(294,1,19),(295,1,19),(296,1,19),(297,1,19),(298,2,19),(299,2,19),(300,2,19),(301,2,19),(302,2,19),(303,2,19),(304,3,19),(306,0,20),(307,0,20),(308,0,20),(309,0,20),(310,0,20),(311,0,20),(312,0,20),(313,0,20),(314,0,20),(315,0,20),(316,0,20),(317,0,20),(318,0,20),(319,1,20),(320,1,20),(321,1,20),(323,0,21),(324,0,21),(325,0,21),(326,0,21),(327,0,21),(328,0,21),(329,0,21),(330,0,21),(331,0,21),(332,0,21),(333,0,21),(334,0,21),(335,0,21),(336,0,21),(337,1,21),(338,1,21),(340,0,22),(341,0,22),(342,0,22),(343,0,22),(344,0,22),(345,0,22),(346,0,22),(347,0,22),(348,1,22),(349,1,22),(350,1,22),(351,0,23),(352,0,23),(353,0,23),(354,0,23),(355,0,23),(356,0,23),(357,0,23),(358,0,23),(359,0,23),(360,0,23),(361,0,23),(362,0,23),(363,0,23),(364,0,23),(365,0,23),(366,0,23),(367,0,23),(368,0,23),(369,0,23),(370,0,23),(371,0,23),(372,0,23),(373,1,23),(374,1,23);
/*!40000 ALTER TABLE `staff_were_enthusiastic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_were_good_at_explaining`
--

DROP TABLE IF EXISTS `staff_were_good_at_explaining`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_were_good_at_explaining` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `response` int(11) DEFAULT NULL,
  `spf_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_2oaolbmxa9p2g44nja2yu2jkf` (`spf_id`),
  CONSTRAINT `FK_2oaolbmxa9p2g44nja2yu2jkf` FOREIGN KEY (`spf_id`) REFERENCES `student_preference_forms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_were_good_at_explaining`
--

LOCK TABLES `staff_were_good_at_explaining` WRITE;
/*!40000 ALTER TABLE `staff_were_good_at_explaining` DISABLE KEYS */;
INSERT INTO `staff_were_good_at_explaining` VALUES (1,0,1),(2,0,1),(3,0,1),(4,0,1),(5,0,1),(6,0,1),(7,0,1),(8,0,1),(9,0,1),(10,0,1),(11,0,1),(12,0,1),(13,0,1),(14,1,1),(15,1,1),(16,1,1),(17,1,1),(18,1,1),(19,0,2),(20,0,2),(21,1,2),(22,1,2),(23,1,2),(24,1,2),(25,1,2),(26,1,2),(27,2,2),(28,0,3),(29,0,3),(30,0,3),(31,1,3),(32,1,3),(33,1,3),(34,1,3),(35,2,3),(36,2,3),(37,2,3),(38,3,3),(39,2,4),(40,3,4),(41,0,5),(42,0,5),(43,1,5),(44,1,5),(45,1,5),(46,2,5),(47,3,5),(48,0,6),(49,0,6),(50,0,6),(51,0,6),(52,0,6),(53,0,6),(54,0,6),(55,1,6),(56,1,6),(57,1,6),(58,1,6),(59,1,6),(60,1,6),(61,2,6),(62,2,6),(63,2,6),(64,2,6),(65,2,6),(66,3,6),(67,3,6),(68,3,6),(69,4,6),(70,0,7),(71,0,7),(72,0,7),(73,1,7),(74,1,7),(75,1,7),(76,1,7),(77,1,7),(78,1,7),(79,2,7),(80,2,7),(81,2,7),(82,3,7),(83,3,7),(84,0,8),(85,0,8),(86,0,8),(87,1,8),(88,1,8),(89,0,9),(90,0,9),(91,0,9),(92,0,9),(93,0,9),(94,0,9),(95,1,9),(96,1,9),(97,1,9),(98,3,9),(99,0,10),(100,0,10),(101,0,10),(102,0,10),(103,0,10),(104,0,10),(105,0,10),(106,1,10),(107,1,10),(108,1,10),(109,2,10),(110,2,10),(111,3,10),(113,0,11),(114,0,11),(115,0,11),(116,0,11),(117,0,11),(118,0,11),(119,0,11),(120,0,11),(121,0,11),(122,1,11),(123,1,11),(124,1,11),(125,1,11),(126,1,11),(127,2,11),(128,2,11),(129,0,12),(130,0,12),(131,0,12),(132,0,12),(133,1,12),(134,1,12),(135,1,12),(136,1,12),(137,1,12),(138,1,12),(139,3,12),(140,3,12);
/*!40000 ALTER TABLE `staff_were_good_at_explaining` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_preference_forms`
--

DROP TABLE IF EXISTS `student_preference_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_preference_forms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module_id` bigint(20) NOT NULL,
  `year` int(11) DEFAULT NULL,
  `average_result` double DEFAULT NULL,
  `cis` double DEFAULT NULL,
  `fewm` double DEFAULT NULL,
  `fowwu` double DEFAULT NULL,
  `mrhs` double DEFAULT NULL,
  `sgawn` double DEFAULT NULL,
  `smsi` double DEFAULT NULL,
  `swe` double DEFAULT NULL,
  `swgae` double DEFAULT NULL,
  `swmq` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_14wvn85ek81qm1e7l338a3o8e` (`module_id`),
  CONSTRAINT `FK_14wvn85ek81qm1e7l338a3o8e` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_preference_forms`
--

LOCK TABLES `student_preference_forms` WRITE;
/*!40000 ALTER TABLE `student_preference_forms` DISABLE KEYS */;
INSERT INTO `student_preference_forms` VALUES (1,30,2014,0.45678888888888886,0.7222,0.7778,0.6111,0.4444,0.2778,0.5,0.1111,0.2778,0.3889),(2,31,2014,0.7901333333333334,0.8889,0.8889,0.8889,0.5556,0.8889,1,0.7778,0.8889,0.3333),(3,32,2014,1.3414111111111113,1.7273,2.2727,0.8,0.9091,1.1818,2.0909,0.9091,1.1818,1),(4,33,2014,1.3888888888888888,0.5,2,1.5,1,1,1.5,1,2.5,1.5),(5,29,2014,0.7658777777777778,0.8571,0.75,1.1429,0.8571,1.1429,0.375,0.125,1.1429,0.5),(6,27,2014,1.378311111111111,1.6364,1.5455,2.0909,1.0909,1.1579,1.6364,0.5,1.3182,1.4286),(7,26,2014,0.8217333333333333,1,0.7857,0.6429,0.6923,0.8462,0.8571,0.5,1.2857,0.7857),(8,25,2014,0.7587333333333333,1.2,1.2,1.2,0.6,0.4,0.8,0.6,0.4,0.4286),(9,24,2014,0.8330222222222221,0.6,1.1,1,1.9,0.2222,0.8,0.4,0.6,0.875),(10,23,2014,0.7132888888888888,0.9231,0.7692,0.6154,0.8462,0.7273,0.6923,0.3846,0.7692,0.6923),(11,17,2014,0.4662,0.5,0.75,1,0.5,0.1333,0.375,0.0625,0.5625,0.3125),(12,16,2014,1.0059,0.9167,0.9167,1.6364,2,1.25,0.5833,0.0833,1,0.6667),(13,30,2013,0.5681500000000002,0.7381,0.9024,0.9286,0.5714,0.5238,0.2143,0.119,1,0.5476),(14,31,2013,0.7595375000000001,0.6,1.1,1.15,0.95,0.5263,0.65,0.45,1,0.65),(15,32,2013,0.9829749999999999,0.9583,1.5417,0.7826,0.9167,0.9091,0.9167,0.7083,1,1.1304),(17,29,2013,0.4791625,0.3333,0.4167,0.75,0.5,0.75,0.3333,0.3333,1,0.4167),(18,27,2013,1.809625,1.8667,1.8966,2.1875,2.2,1.8929,1.7,0.8333,1,1.9),(19,26,2013,1.222225,1.4722,1.6111,1.25,1.0556,1,1.25,0.8056,1,1.3333),(20,25,2013,0.5197875,0.625,0.75,0.8125,0.5,0.5333,0.375,0.1875,1,0.375),(21,24,2013,0.48382500000000006,0.1875,0.6,0.5625,1.125,0.6667,0.1333,0.125,1,0.4706),(22,23,2013,0.5682,0.9091,0.6364,0.5455,0.6364,0.5455,0.3636,0.2727,1,0.6364),(23,17,2013,0.40623750000000003,0.3333,0.5,0.7083,0.375,0.625,0.2917,0.0833,1,0.3333);
/*!40000 ALTER TABLE `student_preference_forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suff_guidance_avail_when_needed`
--

DROP TABLE IF EXISTS `suff_guidance_avail_when_needed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suff_guidance_avail_when_needed` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `response` int(11) DEFAULT NULL,
  `spf_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tgqiowai7q864k3xvulw5u75h` (`spf_id`),
  CONSTRAINT `FK_tgqiowai7q864k3xvulw5u75h` FOREIGN KEY (`spf_id`) REFERENCES `student_preference_forms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=375 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suff_guidance_avail_when_needed`
--

LOCK TABLES `suff_guidance_avail_when_needed` WRITE;
/*!40000 ALTER TABLE `suff_guidance_avail_when_needed` DISABLE KEYS */;
INSERT INTO `suff_guidance_avail_when_needed` VALUES (1,0,1),(2,0,1),(3,0,1),(4,0,1),(5,0,1),(6,0,1),(7,0,1),(8,0,1),(9,0,1),(10,0,1),(11,0,1),(12,0,1),(13,0,1),(14,1,1),(15,1,1),(16,1,1),(17,1,1),(18,1,1),(19,0,2),(20,0,2),(21,1,2),(22,1,2),(23,1,2),(24,1,2),(25,1,2),(26,1,2),(27,2,2),(28,0,3),(29,0,3),(30,0,3),(31,0,3),(32,0,3),(33,1,3),(34,1,3),(35,1,3),(36,3,3),(37,4,3),(38,3,3),(39,1,4),(40,1,4),(41,0,5),(42,0,5),(43,1,5),(44,1,5),(45,1,5),(46,2,5),(47,3,5),(48,0,6),(49,0,6),(50,0,6),(51,0,6),(52,0,6),(53,0,6),(54,1,6),(55,1,6),(56,1,6),(57,1,6),(58,1,6),(59,1,6),(60,2,6),(61,2,6),(62,2,6),(63,2,6),(64,2,6),(65,3,6),(66,3,6),(67,0,7),(68,0,7),(69,0,7),(70,1,7),(71,1,7),(72,1,7),(73,1,7),(74,1,7),(75,1,7),(76,1,7),(77,1,7),(78,1,7),(79,2,7),(80,0,8),(81,0,8),(82,0,8),(83,1,8),(84,1,8),(85,0,9),(86,0,9),(87,0,9),(88,0,9),(89,0,9),(90,0,9),(91,0,9),(92,1,9),(93,1,9),(98,0,10),(99,0,10),(100,0,10),(101,0,10),(102,0,10),(103,1,10),(104,1,10),(105,1,10),(106,1,10),(107,2,10),(108,2,10),(113,0,11),(114,0,11),(115,0,11),(116,0,11),(117,0,11),(118,0,11),(119,0,11),(120,0,11),(121,0,11),(122,0,11),(123,0,11),(124,0,11),(125,0,11),(126,1,11),(127,1,11),(129,0,12),(130,0,12),(131,0,12),(132,0,12),(133,0,12),(134,1,12),(135,1,12),(136,2,12),(137,2,12),(138,2,12),(139,3,12),(140,4,12),(141,0,13),(142,0,13),(143,0,13),(144,0,13),(145,0,13),(146,0,13),(147,0,13),(148,0,13),(149,0,13),(150,0,13),(151,0,13),(152,0,13),(153,0,13),(154,0,13),(155,0,13),(156,0,13),(157,0,13),(158,0,13),(159,0,13),(160,0,13),(161,0,13),(162,0,13),(163,0,13),(164,0,13),(165,0,13),(166,1,13),(167,1,13),(168,1,13),(169,1,13),(170,1,13),(171,1,13),(172,1,13),(173,1,13),(174,1,13),(175,1,13),(176,1,13),(177,1,13),(178,1,13),(179,2,13),(180,2,13),(181,2,13),(182,3,13),(183,0,14),(184,0,14),(185,0,14),(186,0,14),(187,0,14),(188,0,14),(189,0,14),(190,0,14),(191,0,14),(192,0,14),(193,0,14),(194,0,14),(195,1,14),(196,1,14),(197,1,14),(198,1,14),(199,2,14),(200,2,14),(201,2,14),(203,0,15),(204,0,15),(205,0,15),(206,0,15),(207,0,15),(208,0,15),(209,0,15),(210,0,15),(211,0,15),(212,1,15),(213,1,15),(214,1,15),(215,1,15),(216,1,15),(217,1,15),(218,1,15),(219,2,15),(220,2,15),(221,2,15),(222,2,15),(223,2,15),(224,3,15),(227,0,17),(228,0,17),(229,0,17),(230,0,17),(231,0,17),(232,0,17),(233,0,17),(234,1,17),(235,1,17),(236,1,17),(237,2,17),(238,4,17),(239,0,18),(240,1,18),(241,1,18),(242,1,18),(243,1,18),(244,1,18),(245,1,18),(246,1,18),(247,1,18),(248,1,18),(249,1,18),(250,1,18),(251,2,18),(252,2,18),(253,2,18),(254,2,18),(255,2,18),(256,2,18),(257,2,18),(258,2,18),(259,2,18),(260,3,18),(261,3,18),(262,3,18),(263,3,18),(264,4,18),(265,4,18),(266,4,18),(269,0,19),(270,0,19),(271,0,19),(272,0,19),(273,0,19),(274,0,19),(275,0,19),(276,0,19),(277,0,19),(278,0,19),(279,1,19),(280,1,19),(281,1,19),(282,1,19),(283,1,19),(284,1,19),(285,1,19),(286,1,19),(287,1,19),(288,1,19),(289,1,19),(290,1,19),(291,1,19),(292,1,19),(293,1,19),(294,2,19),(295,2,19),(296,2,19),(297,2,19),(298,2,19),(299,2,19),(300,2,19),(301,2,19),(302,2,19),(303,3,19),(305,0,19),(306,0,20),(307,0,20),(308,0,20),(309,0,20),(310,0,20),(311,0,20),(312,0,20),(313,0,20),(314,0,20),(315,0,20),(316,0,20),(317,2,20),(318,2,20),(319,2,20),(320,2,20),(323,0,21),(324,0,21),(325,0,21),(326,0,21),(327,0,21),(328,0,21),(329,0,21),(330,1,21),(331,1,21),(332,1,21),(333,1,21),(334,1,21),(335,1,21),(336,2,21),(337,2,21),(340,0,22),(341,0,22),(342,0,22),(343,0,22),(344,0,22),(345,0,22),(346,1,22),(347,1,22),(348,1,22),(349,1,22),(350,2,22),(351,0,23),(352,0,23),(353,0,23),(354,0,23),(355,0,23),(356,0,23),(357,0,23),(358,0,23),(359,0,23),(360,0,23),(361,0,23),(362,0,23),(363,1,23),(364,1,23),(365,1,23),(366,1,23),(367,1,23),(368,1,23),(369,1,23),(370,1,23),(371,1,23),(372,2,23),(373,2,23),(374,2,23);
/*!40000 ALTER TABLE `suff_guidance_avail_when_needed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available_tag_id` bigint(20) NOT NULL,
  `module_id` bigint(20) NOT NULL,
  `percentage` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cv7nkwwpxesjmbunkq2pqyg53` (`available_tag_id`),
  KEY `FK_8m31hm0ag0dq8k8ceoxh0gxsf` (`module_id`),
  CONSTRAINT `FK_8m31hm0ag0dq8k8ceoxh0gxsf` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`),
  CONSTRAINT `FK_cv7nkwwpxesjmbunkq2pqyg53` FOREIGN KEY (`available_tag_id`) REFERENCES `available_tag` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (1,1,16,50),(2,5,16,25),(3,3,17,40),(4,6,17,60),(5,2,19,65),(6,7,19,35),(7,5,20,100),(8,8,21,50),(9,9,21,50),(10,10,23,70),(11,2,23,30),(12,11,24,40),(13,12,24,40),(14,1,24,20),(15,4,25,30),(16,9,25,70),(17,2,26,65),(18,9,26,35),(19,2,27,33),(20,11,27,34),(21,12,27,33),(22,3,28,50),(23,12,28,30),(24,2,28,20),(25,3,29,34),(26,1,29,33),(27,13,29,33),(28,1,30,50),(29,14,30,50),(30,3,31,50),(31,12,31,50),(32,3,32,30),(33,1,32,36),(34,12,32,34),(35,12,33,50),(36,3,33,50),(37,1,34,100),(38,15,16,25);
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `university`
--

DROP TABLE IF EXISTS `university`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `university` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `university`
--

LOCK TABLES `university` WRITE;
/*!40000 ALTER TABLE `university` DISABLE KEYS */;
INSERT INTO `university` VALUES (1,'University of Leeds');
/*!40000 ALTER TABLE `university` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `school_id` bigint(20) NOT NULL,
  `university_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`username`),
  KEY `FK_bslnxdcwuprla5l6mn6jhuwc7` (`school_id`),
  KEY `FK_rebo2xbcgxy60wi2e79gd4o86` (`university_id`),
  KEY `FK_7f4qfe09fxsen6c7l731phby5` (`course_id`),
  CONSTRAINT `FK_7f4qfe09fxsen6c7l731phby5` FOREIGN KEY (`course_id`) REFERENCES `available_course` (`id`),
  CONSTRAINT `FK_bslnxdcwuprla5l6mn6jhuwc7` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`),
  CONSTRAINT `FK_rebo2xbcgxy60wi2e79gd4o86` FOREIGN KEY (`university_id`) REFERENCES `university` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('user',1,'John','','Wick',1,1,1,2),('user2',1,'Bruce','','Banner',1,1,2,2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_past_modules`
--

DROP TABLE IF EXISTS `users_past_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_past_modules` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module_id` bigint(20) NOT NULL,
  `user_username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_lh87odp3pxo6uqa19rnq8dcbg` (`module_id`),
  KEY `FK_kmdpl4p6gf61ispmrlmicyook` (`user_username`),
  CONSTRAINT `FK_kmdpl4p6gf61ispmrlmicyook` FOREIGN KEY (`user_username`) REFERENCES `users` (`username`),
  CONSTRAINT `FK_lh87odp3pxo6uqa19rnq8dcbg` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_past_modules`
--

LOCK TABLES `users_past_modules` WRITE;
/*!40000 ALTER TABLE `users_past_modules` DISABLE KEYS */;
INSERT INTO `users_past_modules` VALUES (17,22,'user2'),(18,1,'user2'),(19,2,'user2'),(20,3,'user2'),(21,6,'user2'),(22,7,'user2'),(23,10,'user2'),(24,11,'user2'),(25,12,'user2'),(26,13,'user2'),(27,14,'user2'),(95,2,'user'),(96,3,'user'),(97,4,'user'),(98,5,'user'),(99,7,'user'),(100,8,'user'),(101,9,'user'),(102,11,'user'),(103,12,'user'),(104,15,'user'),(105,18,'user');
/*!40000 ALTER TABLE `users_past_modules` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-11 11:47:24
